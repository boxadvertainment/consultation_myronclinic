var gulp     = require('gulp');
var elixir   = require('laravel-elixir');
var dotenv   = require('dotenv').load();
var _        = require('underscore');
var imagemin = require('gulp-imagemin');
var pngquant = require('imagemin-pngquant');
var Task     = elixir.Task;
var config   = elixir.config;

require('./elixir-extensions');


elixir.extend('images', function(options) {
    config.img = {
        folder: 'img',
        outputFolder: config.versioning.buildFolder + '/img'
    };

    options = _.extend({
        progressive: true,
        interlaced : true,
        svgoPlugins: [{removeViewBox: false, cleanupIDs: false}],
        use: [pngquant()]
    }, options);

    new Task('imagemin', function () {
        var paths = new elixir.GulpPaths()
            .src(config.get('public.img.folder'))
            .output(config.get('public.img.outputFolder'));

        return gulp.src(paths.src.path)
            .pipe(imagemin(options))
            .on('error', function(e) {
                new elixir.Notification().error(e, 'Minifying Images Failed!');
                this.emit('end');
            })
            .pipe(gulp.dest(paths.output.path));
    });
});
