'use strict';

(function ($) {
  "use strict";

  if (APP_ENV == 'production') {
    console.log = function () {};
  }

  $('.loader').addClass('hide');

  function showLoader() {
    $('.loader').removeClass('hide');
    $('body').css('overflow', 'hidden');
  }

  function hideLoader() {
    $('.loader').addClass('hide');
    $('body').css('overflow', 'visible');
  }

  function showPage(selector) {
    $(selector).removeClass('hide').siblings().addClass('hide');
    facebookUtils.resizeCanvas();
  }

  $.ajaxSetup({
    error: function error(jqXHR) {
      console.log('jqXHR : ', jqXHR);
      if (/<html>/i.test(jqXHR.responseText)) sweetAlert('Oups!', 'Une erreur serveur s\'est produite, veuillez réessayer ultérieurement.', 'error');else sweetAlert({
        title: "Oups!",
        text: jqXHR.responseText,
        type: "error",
        html: true
      });
    }
  });

  function hasHtml5Validation() {
    return typeof document.createElement('input').checkValidity === 'function';
  }

  var disabled = false;

  $('#consultation-form-modal').submit(function (e) {
    var $this = $(this);
    if (disabled) return false;

    $('button.submit').button('loading');

    disabled = true;

    // if selected intervention est chirugie esthetique send form to drdjemal DB
    var chirugie = 'CHIRURGIE ESTHETIQUE';
    var URL_drdjemal = 'http://www.dr-djemal.com/submitConsultation';

    var formData = $this.serializeArray();
    formData.push({
      name: "from",
      value: $('#intervention').val() == chirugie ? true : false
    });

    $.ajax({
      url: $('#intervention').val() == chirugie ? URL_drdjemal : $this.attr('action'),
      method: 'POST',
      data: formData
    }).done(function (response) {
      location.href = 'merci/' + response.message;
      disabled = false;
      $('button.submit').button('reset');
    });

    e.preventDefault();
  });

  $('.branch-btn').hover(function (event) {
    event.preventDefault();

    $('.branch-btn').parent('li').removeClass('active');
    $(this).parent('li').addClass('active');

    $('.treatments-list').stop().slideUp(100);
    $(this).parent().find('.treatments-list').stop().delay(100).slideDown(300);
  });

  var input = document.querySelector("#phone");
  var iti = window.intlTelInput(input, {
    initialCountry: "auto",
    separateDialCode: true,
    geoIpLookup: function geoIpLookup(success, failure) {
      $.get("https://ipinfo.io", function () {}, "jsonp").always(function (resp) {
        var countryCode = resp && resp.country ? resp.country : "";
        success(countryCode);
      });
    },
    customPlaceholder: function customPlaceholder(selectedCountryPlaceholder, selectedCountryData) {
      return "e.g. " + selectedCountryPlaceholder;
    }
  });

  $("#country").countrySelect({
    defaultCountry: "tn"
  });
})(jQuery);
//# sourceMappingURL=main.js.map
