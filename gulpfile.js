var elixir   = require('laravel-elixir');

require('./elixir-extensions');

elixir(function(mix) {
    mix
        .sass('main.scss', 'public/css/main.css')
        .babel([
          //'facebookUtils.js',
          'main.js'
        ], 'public/js/main.js')
        .scripts([
            // bower:js
            'bower_components/jquery/dist/jquery.js',
            'bower_components/bootstrap-sass/assets/javascripts/bootstrap.js',
            'bower_components/sweetalert/dist/sweetalert.min.js',
            'bower_components/jquery-colorbox/jquery.colorbox.js',
            'bower_components/country-select-js/build/js/countrySelect.js',
            'bower_components/intl-tel-input/build/js/intlTelInput.js',
            'bower_components/intl-tel-input/build/js/utils.js',
            // endbower
        ], 'public/js/vendor.js', 'bower_components')
        .styles([
            // bower:css
            'bower_components/sweetalert/dist/sweetalert.css',
            'bower_components/country-select-js/build/css/countrySelect.css',
            'bower_components/intl-tel-input/build/css/intlTelInput.css',
            // endbower
            "font-awesome/css/font-awesome.min.css",
            "jquery-colorbox/example2/colorbox.css"
        ], 'public/css/vendor.css', 'bower_components')
        //.version(['css/main.css', 'js/main.js'])
        .images()
        .copy('vendor/bower_components/font-awesome/fonts', 'public/fonts')
        .copy('vendor/bower_components/bootstrap-sass/assets/fonts', 'public/fonts')
        .copy('public/fonts', 'public/build/fonts')
        .browserSync({
            proxy: process.env.APP_URL
        });
});
