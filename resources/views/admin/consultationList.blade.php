@extends('admin/layout')
@section('content')

    <h3 class="page-header">
        <div class="pull-right">
            <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#consultationModal">
                <i class="fa fa-plus"></i>
                Ajouter une consultation
            </button>
        </div>
        Consultations ({{ $count }})
    </h3>
    <table class="table-sort table-sort-search">
        <thead>
        <th>#</th>
        <th class="table-sort">Date demande</th>
        <th class="table-sort">Nom et prénom</th>
        <th class="table-sort">Email</th>
        <th>Téléphone</th>
        <th class="table-sort">Intervention</th>
        <th>Message</th>
        <th>Status</th>
        <th class="table-sort">Pays</th>
        <th>Actions Dossier</th>
        </thead>
        <tbody>
        @foreach($consultations as $key => $consultation)
            <tr>
                <td>{{ ($consultations->currentPage() - 1) * 100 + $key + 1}}</td>
                <td>{{ $consultation->created_at->format('Y/m/d') }}</td>
                <td>{{ $consultation->name}}</td>
                <td><a class="btn btn-default" href="mailto:{{ $consultation->email }}">{{ $consultation->email }}</a>
                </td>
                <td>{{ $consultation->phone }}</td>
                <td>{{ $consultation->intervention }}</td>
                <td>
                    <div data-container="body" data-toggle="popover" data-trigger="hover" data-placement="bottom"
                         data-content="{{ $consultation->message }}">
                        {{ str_limit($consultation->message, 30) }}
                    </div>
                </td>
                <td data-id="{{ $consultation->id }}">
                    @if(Auth::user()->role == 'user')
                        <span class="label label-{{ $status[$consultation->status][1] }}">{{ $status[$consultation->status][0] }}</span>
                    @else
                        <div class="btn-group">
                            <button class="btn btn-{{ $status[$consultation->status][1] }} btn-sm dropdown-toggle"
                                    type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                {{ $status[$consultation->status][0] }} {{ @$consultation->date_status[$consultation->status] }}
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu">
                                @foreach($status as $key => $stat)
                                    @if($key !==  $consultation->status)
                                        <li><a href="#" data-val="{{ $key }}"
                                               class="bg-{{ $stat[1] }}">{{ $stat[0] }} {{ @$consultation->date_status[$key] }}</a>
                                        </li>
                                    @endif
                                @endforeach
                            </ul>
                        </div>
                    @endif
                </td>
                <td>
                    @if ( $consultation->pays != '' )
                        <span class="bfh-countries" data-country="{{ $consultation->pays }}" data-flags="true"></span>
                    @endif
                </td>
                <td>
                    @if($consultation->passion_id)
                        <div class="btn-group">
                            <a href="{{ action('Admin\AdminController@consultationEdit', ['id' => $consultation->id]) }}"
                               class="btn btn-info btn-sm">Ouvrir le dossier</a>
                            <button type="button" class="btn btn-info btn-sm" data-toggle="popover"
                                    title="Identifiants de connexion"
                                    data-content="<b>Email:</b> {{ $consultation->email }}<br><b>Mot de passe:</b> {{ $consultation->passion->_password }}"
                                    data-placement="left" data-container="body" data-html="true">
                                <i class="fa fa-key"></i>
                            </button>
                            <a href="{{ action('Admin\AdminController@consultationToPdf', ['id' => $consultation->id]) }}"
                               class="btn btn-info btn-sm">Générate PDF</a>
                            {{--<button type="button" class="btn btn-info btn-sm" data-toggle="popover"
                                    title="Identifiants de connexion"
                                    data-content="<b>Email:</b> {{ $consultation->email }}<br><b>Mot de passe:</b> {{ $consultation->passion->_password }} <br> <a href='{{ action('Admin\AdminController@EmailUser', ['email' => $consultation->email , 'password' => $consultation->passion->_password , 'name' => $consultation->name ]) }}' class='btn btn-info'>Envoyer</a>"
                                    data-placement="left" data-container="body" data-html="true">
                                <i class="fa fa-key"></i>
                            </button>--}}

                        </div>
                    @else
                        <button class="btn btn-success btn-sm create-patient" data-id="{{ $consultation->id }}">Créer un
                            nouveau patient
                        </button>
                    @endif
                    <a href="{{ action('Admin\AdminController@deleteConsultation', ['id' => $consultation->id]) }}"
                        class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    <div class="text-center">
        {!! $consultations->render() !!}
    </div>
@endsection