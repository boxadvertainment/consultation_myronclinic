<!DOCTYPE html>
<html lang="fr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Box">
    <title>Admin Panel - myronclinic</title>
    <!-- Metta CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <link rel="stylesheet" href="{{ asset('css/admin/bootstrap.min.css') }}">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ asset('css/bootstrap-formhelpers.css') }}">
    <style>
        .page-header {
            color: #edaf4f;
            text-align: center;
        }
        .wrapper{
            width: 596px;
            margin: 0 auto;;
        }
    </style>
</head>
<body>
<div class="wrapper">
    @yield('content')
</div>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="{{ asset('js/bootstrap-formhelpers.js') }}"></script>
</body>
</html>
