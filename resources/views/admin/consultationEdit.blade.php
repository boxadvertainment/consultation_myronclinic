@extends('admin/layout')
@section('content')
    <h1 class="page-header">
        Détail consultation
    </h1>
    <form action="" method="post" class="consultation-form" enctype="multipart/form-data">
        {{ csrf_field() }}
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#demande" class="color-1" aria-controls="demande" role="tab"
                                                      data-toggle="tab">Votre
                    demande</a></li>
            <li role="presentation"><a href="#antecedents" class="color-1" aria-controls="antecedents" role="tab"
                                       data-toggle="tab">Vos
                    antécédents médicaux</a></li>
            <li role="presentation">
                <a href="#photos" class="color-1" aria-controls="photos" role="tab" data-toggle="tab">Vos Rapports</a>
            </li>
            @if (Auth::user()->role != 'user')
                <li role="presentation"><a href="#voyage" class="color-1" aria-controls="voyage" role="tab"
                                           data-toggle="tab">Votre voyage</a></li>
                <li role="presentation"><a href="#diagnostic" class="color-2" aria-controls="diagnostic" role="tab"
                                           data-toggle="tab">Diagnostic</a></li>
                <li role="presentation"><a href="#devis" class="color-2" aria-controls="devis" role="tab"
                                           data-toggle="tab">Votre devis</a></li>
                <li role="presentation"><a href="#post-operatoire" class="color-2" aria-controls="photos" role="tab"
                                           data-toggle="tab">Post opératoire</a></li>
                <li role="presentation"><a href="#planning" class="color-2" aria-controls="devis" role="tab"
                                           data-toggle="tab">Planning</a></li>
                <li role="presentation"><a href="#message" class="color-2" aria-controls="photos" role="tab"
                                           data-toggle="tab">Message / Infos</a></li>
            @endif
        </ul>
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="demande">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="name">Nom et prénom: ( الإسم و اللقب )</label>
                                        <input type="text" id="name" name="name" class="form-control"
                                               value="{{ $consultation->name }}">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="name">Age: ( العمر )</label>
                                        <input type="number" id="age" name="age" class="form-control"
                                               value="{{ @$dossier['age'] }}">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="nationality">Nationalité: ( الجنسية )</label>
                                        <input type="text" id="nationality" class="form-control" name="nationality"
                                               value="{{ @$dossier['nationality'] }}">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="country">Pays: ( البلد )</label>
                                        <div class="bfh-selectbox bfh-countries"
                                             data-country="{{ $consultation->pays }}" data-flags="true"
                                             data-filter="true"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="phone">Tél. Mobile: ( رقم الهاتف )</label>
                                        <input type="text" id="phone" class="form-control" name="phone"
                                               value="{{ $consultation->phone }}">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="email">Email: ( البريد الإلكتروني ؟ )</label>
                                        <input type="email" id="email" class="form-control" name="email"
                                               value="{{ $consultation->email }}">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="viber">Viber</label>
                                        <input type="text" id="viber" class="form-control" name="viber"
                                               value="{{ @$dossier['viber'] }}">
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="whatsapp">WhatsApp</label>
                                        <input type="text" id="whatsapp" class="form-control" name="whatsapp"
                                               value="{{ @$dossier['whatsapp'] }}">
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="skype">Skype</label>
                                        <input type="text" id="skype" class="form-control" name="skype"
                                               value="{{ @$dossier['skype'] }}">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="reason">Raison de la demande: ( سبب الطلب )</label>
                                      <textarea name="reason" id="reason" cols="30" rows="4"
                                                class="form-control">{{ $consultation->message }}</textarea>
                            </div>
                            <div class="form-group">
                                <label for="poids">Votre Poids : ( الوزن )</label>
                                <input type="text" id="poids" class="form-control" name="poids"
                                       value="{{ @$dossier['poids'] }}">
                            </div>
                            <div class="form-group">
                                <label for="sexe">Sexe : ( الجنس )</label>
                                <div class="radio">
                                    <label class="radio-inline">
                                        <input type="radio" name="sexe"
                                               value="Femme" {{ (@$dossier['sexe'] == 'Femme') ? 'checked': '' }}> Femme
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="sexe"
                                               value="Homme" {{ (@$dossier['sexe'] == 'Homme') ? 'checked': '' }}> Homme
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="remarques">Remarques: ( ملاحظات )</label>
                                      <textarea name="remarques" id="remarques" cols="30" rows="4"
                                                class="form-control">{{ $dossier['remarques'] }}</textarea>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="antecedents">
                        <div class="panel-body">
                            <div class="form-group">
                                <label for="consulteChirurgien">Consommez-vous ou avez vous consommé du tabac
                                    ? ( هل تستهلك التبغ؟ )</label>
                                <div class="radio">
                                    <label class="radio-inline">
                                        <input type="radio" name="consommationTabac"
                                               value="Oui" {{ (@$dossier['consommationTabac'] == 'Oui') ? 'checked': '' }}>
                                        Oui
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="consommationTabac"
                                               value="Non" {{ (@$dossier['consommationTabac'] == 'Non') ? 'checked': '' }}>
                                        Non
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="depuisAge">Depuis quel âge et â quelles fréquences ? ( كم؟ )</label>
                                <input type="text" id="depuisAge" class="form-control" name="depuisAge"
                                       value="{{ @$dossier['depuisAge'] }}">
                            </div>
                            <div class="form-group">
                                <label>Avez vous arrêté de fumer ?</label>
                                <div class="radio">
                                    <label class="radio-inline">
                                        <input type="radio" name="arreterFumer"
                                               value="Oui" {{ (@$dossier['arreterFumer'] == 'Oui') ? 'checked': '' }}>
                                        Oui
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="arreterFumer"
                                               value="Non" {{ (@$dossier['arreterFumer'] == 'Non') ? 'checked': '' }}>
                                        Non
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Consommez vous de l'alcool ? ( هل تستهلك الكوحول؟ )</label>
                                <div class="radio">
                                    <label class="radio-inline">
                                        <input type="radio" name="consommationAlcool"
                                               value="Oui" {{ (@$dossier['consommationAlcool'] == 'Oui') ? 'checked': '' }}>
                                        Oui
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="consommationAlcool"
                                               value="Non" {{ (@$dossier['consommationAlcool'] == 'Non') ? 'checked': '' }}>
                                        Non
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="alcoolfrequence">Si Oui, et â quelles fréquences ? (  كم؟ )</label>
                                <input type="text" id="alcoolfrequence" class="form-control" name="alcoolfrequence"
                                       value="{{ @$dossier['alcoolfrequence'] }}">
                            </div>
                            <div class="form-group">
                                <label>Prenez vous des médicaments ? ( هل تتناول أدوية؟ )</label>
                                <div class="radio">
                                    <label class="radio-inline">
                                        <input type="radio" name="prisMedicament"
                                               value="Oui" {{ (@$dossier['prisMedicament'] == 'Oui') ? 'checked': '' }}>
                                        Oui
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="prisMedicament"
                                               value="Non" {{ (@$dossier['prisMedicament'] == 'Non') ? 'checked': '' }}>
                                        Non
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="medicamentFrequence">Si Oui, et â quelles fréquences ? ( ماهي )</label>
                                <input type="text" id="medicamentFrequence" class="form-control" name="medicamentFrequence"
                                       value="{{ @$dossier['medicamentFrequence'] }}">
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="nbGrossesses">Combien de grossesses avez vous eues ? ( عدد الحملات )</label>
                                        <select name="nbGrossesses" class="form-control">
                                                @for($i=0; $i<6; $i++)
                                                    <option value="{{ $i }}"
                                                            @if( @$dossier['nbGrossesses'] == $i) selected @endif>{{ $i }}</option>
                                                @endfor
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="nbEnfants">Combien d'enfants avez vous eus ? ( عدد الأطفال )</label>
                                        <select name="nbEnfants" id="nbEnfants" class="form-control">
                                                @for($i=0; $i<6; $i++)
                                                    <option value="{{ $i }}"
                                                            @if( @$dossier['nbGrossesses'] == $i) selected @endif>{{ $i }}</option>
                                                @endfor
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="nbCesarienne">Combien de césarienne(s) avez vous eu ? ( عدد القيسريات )</label>
                                        <select name="nbCesarienne" id="nbCesarienne" class="form-control">
                                            @for($i=0; $i<6; $i++)
                                                <option value="{{ $i }}"
                                                        @if( @$dossier['nbCesarienne'] == $i) selected @endif>{{ $i }}</option>
                                            @endfor
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Etes vous allergique ? ( هل لديك حسسيات؟ )</label>
                                <div class="radio">
                                    <label class="radio-inline">
                                        <input type="radio" name="allergique"
                                               value="Oui" {{ (@$dossier['allergique'] == 'Oui') ? 'checked': '' }}> Oui
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="allergique"
                                               value="Non" {{ (@$dossier['allergique'] == 'Non') ? 'checked': '' }}> Non
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="allergique"
                                               value="unknown" {{ (@$dossier['allergique'] == 'unknown') ? 'checked': '' }}>
                                        Ne sait pas
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="remarques">Si Oui , â quoi ? (merci de préciser) ( حدد ؟ )</label>
                                      <textarea name="autresAllergies" id="autresAllergies" cols="30" rows="4"
                                                class="form-control">{{ @$dossier['autresAllergies'] }}</textarea>
                            </div>
                            <div class="form-group">
                                <label>Etes vous allergique à un médicament ? ( هل لديك حسسيات لأدوية معينة؟ )</label>
                                <div class="radio">
                                    <label class="radio-inline">
                                        <input type="radio" name="allergiqueMedicament"
                                               value="Oui" {{ (@$dossier['allergiqueMedicament'] == 'Oui') ? 'checked': '' }}>
                                        Oui
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="allergiqueMedicament"
                                               value="Non" {{ (@$dossier['allergiqueMedicament'] == 'Non') ? 'checked': '' }}>
                                        Non
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="remarques">si oui, lequel(s) ? ( ما هي؟ )</label>
                                      <textarea name="medicamentAllergique" id="medicamentAllergique" cols="30" rows="4"
                                                class="form-control">{{ @$dossier['medicamentAllergique'] }}</textarea>
                            </div>
                            <div class="form-group">
                                <label>Etes vous allergique au latex ? ( هل لديك حسسيات للبن الشجر؟ )</label>
                                <div class="radio">
                                    <label class="radio-inline">
                                        <input type="radio" name="allergiqueLatex"
                                               value="Oui" {{ (@$dossier['allergiqueLatex'] == 'Oui') ? 'checked': '' }}>
                                        Oui
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="allergiqueLatex"
                                               value="Non" {{ (@$dossier['allergiqueLatex'] == 'Non') ? 'checked': '' }}>
                                        Non
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="allergiqueLatex"
                                               value="unknown" {{ (@$dossier['allergiqueLatex'] == 'unknown') ? 'checked': '' }}>
                                        Ne sait pas
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Souffrez vous d 'hypertension artérielle ? ( هل تعاني من ضغط مرتفع؟ )</label>
                                <div class="radio">
                                    <label class="radio-inline">
                                        <input type="radio" name="hypertensionArterielle"
                                               value="Oui" {{ (@$dossier['hypertensionArterielle'] == 'Oui') ? 'checked': '' }}>
                                        Oui
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="hypertensionArterielle"
                                               value="Non" {{ (@$dossier['hypertensionArterielle'] == 'Non') ? 'checked': '' }}>
                                        Non
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="hypertensionArterielle"
                                               value="unknown" {{ (@$dossier['hypertensionArterielle'] == 'unknown') ? 'checked': '' }}>
                                        Ne sait pas
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Souffrez vous de diabète ? ( هل تعاني من المرض السكري؟ )</label>
                                <div class="radio">
                                    <label class="radio-inline">
                                        <input type="radio" name="diabete"
                                               value="Oui" {{ (@$dossier['diabete'] == 'Oui') ? 'checked': '' }}> Oui
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="diabete"
                                               value="Non" {{ (@$dossier['diabete'] == 'Non') ? 'checked': '' }}> Non
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="diabete"
                                               value="unknown" {{ (@$dossier['diabete'] == 'unknown') ? 'checked': '' }}>
                                        Ne sait pas
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Souffrez vous du cholestérol ? ( هل تعاني من الكولسترول؟ )</label>
                                <div class="radio">
                                    <label class="radio-inline">
                                        <input type="radio" name="cholesterol"
                                               value="Oui" {{ (@$dossier['cholesterol'] == 'Oui') ? 'checked': '' }}>
                                        Oui
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="cholesterol"
                                               value="Non" {{ (@$dossier['cholesterol'] == 'Non') ? 'checked': '' }}>
                                        Non
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="cholesterol"
                                               value="unknown" {{ (@$dossier['cholesterol'] == 'unknown') ? 'checked': '' }}>
                                        Ne sait pas
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Avez vous déjà présenté un caillot dans une veine de la jambe (phlébite) ? ( هل كان لديك أي وقت مضى جلطة في الوريد الساق (وريدي) )</label>
                                <div class="radio">
                                    <label class="radio-inline">
                                        <input type="radio" name="phlebite"
                                               value="Oui" {{ (@$dossier['phlebite'] == 'Oui') ? 'checked': '' }}> Oui
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="phlebite"
                                               value="Non" {{ (@$dossier['phlebite'] == 'Non') ? 'checked': '' }}> Non
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="phlebite"
                                               value="unknown" {{ (@$dossier['phlebite'] == 'unknown') ? 'checked': '' }}>
                                        Ne sait pas
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Avez vous déjà fait une dépression ? ( هل تعاني من الكآبة ؟ )</label>
                                <div class="radio">
                                    <label class="radio-inline">
                                        <input type="radio" name="depression"
                                               value="Oui" {{ (@$dossier['depression'] == 'Oui') ? 'checked': '' }}> Oui
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="depression"
                                               value="Non" {{ (@$dossier['depression'] == 'Non') ? 'checked': '' }}> Non
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>si oui, êtes vous actuellement sous antidépresseur ? ( هل تستعمل أدوية ضد الكٱبة ؟ )</label>
                                <div class="radio">
                                    <label class="radio-inline">
                                        <input type="radio" name="antidepresseur"
                                               value="Oui" {{ (@$dossier['antidepresseur'] == 'Oui') ? 'checked': '' }}>
                                        Oui
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="antidepresseur"
                                               value="Non" {{ (@$dossier['antidepresseur'] == 'Non') ? 'checked': '' }}>
                                        Non
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Etes vous atteint(e) de maladies virales (HIV, Hépatite) ? ( هل تعاني من الأمراض الفيروسية ؟ )</label>
                                <div class="radio">
                                    <label class="radio-inline">
                                        <input type="radio" name="maladiesVirales"
                                               value="Oui" {{ (@$dossier['maladiesVirales'] == 'Oui') ? 'checked': '' }}>
                                        Oui
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="maladiesVirales"
                                               value="Non" {{ (@$dossier['maladiesVirales'] == 'Non') ? 'checked': '' }}>
                                        Non
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="maladiesVirales"
                                               value="unknown" {{ (@$dossier['maladiesVirales'] == 'unknown') ? 'checked': '' }}>
                                        Ne sait pas
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="remarques">si oui, lesquelles ? ( ما هي؟ )</label>
                                      <textarea name="maladiesViralesRemarques" id="maladiesViralesRemarques" cols="30"
                                                rows="4"
                                                class="form-control">{{ @$dossier['maladiesViralesRemarques'] }}</textarea>
                            </div>
                            <div class="form-group">
                                <label for="autresAntecedents">Autres antécédents médicaux (Merci de préciser) ( هل لديك سوابق أخرى؟ )</label>
                                      <textarea name="autresAntecedents" id="autresAntecedents" cols="30" rows="4"
                                                class="form-control">{{ @$dossier['autresAntecedents'] }}</textarea>
                            </div>
                            <div class="form-group">
                                <label>Avez vous déjà eu des interventions chirurgicales ? ( هل أديت عماليات جراحية في السابق؟ )</label>
                                <div class="radio">
                                    <label class="radio-inline">
                                        <input type="radio" name="interventionsChirurgicales"
                                               value="Oui" {{ (@$dossier['interventionsChirurgicales'] == 'Oui') ? 'checked': '' }}>
                                        Oui
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="interventionsChirurgicales"
                                               value="Non" {{ (@$dossier['interventionsChirurgicales'] == 'Non') ? 'checked': '' }}>
                                        Non
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="interventionsChirurgicalesRemarques">si oui, lesquelles ? ( حدد )</label>
                                      <textarea name="interventionsChirurgicalesRemarques"
                                                id="interventionsChirurgicalesRemarques" cols="30" rows="4"
                                                class="form-control">{{ @$dossier['interventionsChirurgicalesRemarques'] }}</textarea>
                            </div>
                            <div class="form-group">
                                <label>Avez vous déjà eu des interventions de chirurgie esthétique ? ( هل أديت عماليات تجميل في السابق؟ )</label>
                                <div class="radio">
                                    <label class="radio-inline">
                                        <input type="radio" name="interventionsEsthetique"
                                               value="Oui" {{ (@$dossier['interventionsEsthetique'] == 'Oui') ? 'checked': '' }}>
                                        Oui
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="interventionsEsthetique"
                                               value="Non" {{ (@$dossier['interventionsEsthetique'] == 'Non') ? 'checked': '' }}>
                                        Non
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="interventionsEsthetiqueRemarques">si oui, lesquelles ? ( حدد )</label>
                                      <textarea name="interventionsEsthetiqueRemarques"
                                                id="interventionsEsthetiqueRemarques" cols="30" rows="4"
                                                class="form-control">{{ @$dossier['interventionsEsthetiqueRemarques'] }}</textarea>
                            </div>

                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="photos">
                        <div class="panel-body">
                            @for($i=0; $i<3; $i++)
                                <div class="row">
                                    @for($j=1; $j<4; $j++)
                                        <div class="col-md-4">
                                            <p><img class="img-thumbnail" style="height: 150px;"
                                                    src="{{  @$dossier['image' . ( $i * 3 + $j)]  ? route('getPhoto', ['filename' => $dossier['image' . ( $i * 3 + $j)]]) : "data:image/svg+xml;charset=utf-8,%3Csvg%20xmlns%3D'http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg'%20xmlns%3Axlink%3D'http%3A%2F%2Fwww.w3.org%2F1999%2Fxlink'%20viewBox%3D'0%200%201400%20933'%3E%3Cdefs%3E%3Csymbol%20id%3D'a'%20viewBox%3D'0%200%2090%2066'%20opacity%3D'0.3'%3E%3Cpath%20d%3D'M85%205v56H5V5h80m5-5H0v66h90V0z'%2F%3E%3Ccircle%20cx%3D'18'%20cy%3D'20'%20r%3D'6'%2F%3E%3Cpath%20d%3D'M56%2014L37%2039l-8-6-17%2023h67z'%2F%3E%3C%2Fsymbol%3E%3C%2Fdefs%3E%3Cuse%20xlink%3Ahref%3D'%23a'%20width%3D'20%25'%20x%3D'40%25'%2F%3E%3C%2Fsvg%3E" }}">
                                            </p>
                                            <div class="form-group">
                                                <input type="file" name="image{{ ( $i * 3) + $j }}">
                                                <input type="hidden" name="oldImage{{ ( $i * 3) + $j }}"
                                                       value="{{  @$dossier['image' . ( $i * 3 + $j)] }}">
                                            </div>
                                        </div>
                                    @endfor
                                </div>
                            @endfor
                        </div>
                    </div>
                    @if (Auth::user()->role != 'user')
                        <div role="tabpanel" class="tab-pane" id="voyage">
                            <div class="panel-body">
                                <div class="form-group">
                                    <label for="formuleVoyage">Hôtel:</label>
                                    <select name="formuleVoyage" id="formuleVoyage" class="form-control">
                                        <option value="Hotel Myron"
                                                @if(@$dossier['formuleVoyage'] == 'Hotel Myron') selected @endif>Hotel
                                            Myron
                                        </option>
                                        <option value="Hotel El Mouradi"
                                                @if(@$dossier['formuleVoyage'] == 'Hotel El Mouradi') selected @endif>
                                            Hotel
                                            El Mouradi
                                        </option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Avez vous un passeport en cours de validité ?</label>
                                    <div class="radio">
                                        <label class="radio-inline">
                                            <input type="radio" name="passeport" value="Oui"
                                                   @if(@$dossier['passeport'] == 'Oui') checked @endif> Oui
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="passeport" value="Non"
                                                   @if(@$dossier['passeport'] == 'Non') checked @endif> Non
                                        </label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="arrangement">Arrangement</label>
                                            <select name="arrangement" id="arrangement" class="form-control">
                                                <option value="pc"
                                                        @if(@$dossier['arrangement'] == 'pc') selected @endif>
                                                    Pension Compléte
                                                </option>
                                                <option value="dp"
                                                        @if(@$dossier['arrangement'] == 'dp') selected @endif>
                                                    Demi pension
                                                </option>
                                                <option value="lpd"
                                                        @if(@$dossier['arrangement'] == 'lpd') selected @endif>
                                                    Lit et petit déjeuner
                                                </option>
                                            </select>
                                            <p class="help-block">(la pension n’inclut pas les dépenses personnelles
                                                telles
                                                que
                                                téléphone,
                                                boissons...)</p>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="typeChambre">Type de chambre</label>
                                            <select name="typeChambre" id="typeChambre" class="form-control">
                                                <option value="single"
                                                        @if(@$dossier['typeChambre'] == 'single') selected @endif>Single
                                                </option>
                                                <option value="double"
                                                        @if(@$dossier['typeChambre'] == 'double') selected @endif>Double
                                                </option>
                                                <option value="triple"
                                                        @if(@$dossier['typeChambre'] == 'triple') selected @endif>Triple
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Date d'arrivée :</label>
                                            <input type="date" class="form-control" name="dateArrivee"
                                                   value="{{ @$dossier['dateArrivee'] }}">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Date de départ :</label>
                                            <input type="date" class="form-control" name="dateDepart"
                                                   value="{{ @$dossier['dateDepart'] }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="nbAdultes">Nombre d'adultes :</label>
                                            <select name="nbAdultes" class="form-control">
                                                @for($i=0; $i<11; $i++)
                                                    <option value="{{ $i }}"
                                                            @if( @$dossier['nbAdultes'] == $i) selected @endif>{{ $i }}</option>
                                                @endfor
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="nbBebes">Nombre de Bébés de 0 à 2 ans :</label>
                                            <select name="nbBebes" class="form-control">
                                                @for($i=0; $i<11; $i++)
                                                    <option value="{{ $i }}"
                                                            @if( @$dossier['nbBebes'] == $i) selected @endif>{{ $i }}</option>
                                                @endfor
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="nbEnfants">Nombre d'enfants de 2 à 12 ans :</label>
                                            <select name="nbEnfants" class="form-control">
                                                @for($i=0; $i<11; $i++)
                                                    <option value="{{ $i }}"
                                                            @if( @$dossier['nbEnfants'] == $i) selected @endif>{{ $i }}</option>
                                                @endfor
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <p><strong>Assistance :</strong></p>
                                        <p>Nous vous prenons en charge depuis votre arrivée à l'aéroport et jusqu'à
                                            votre
                                            départ:</p>
                                        <ul>
                                            <li>Accueil</li>
                                            <li>Transferts Hôtel/Clinique et Hôtel/Aéroport</li>
                                            <li>Rendez-vous médicaux</li>
                                        </ul>
                                        <br>
                                    </div>
                                </div>

                                <div class="panel panel-warning">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">Vos vols</h3>
                                    </div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-7">
                                                <table border="0" cellpadding="2" cellspacing="1"
                                                       class="table table-bordered">
                                                    <tbody>
                                                    <tr valign="top">
                                                        <td colspan="4" class="info"><b>Vol aller</b></td>
                                                    </tr>
                                                    <tr align="center" valign="top" class="fdl2">
                                                        <td>Date</td>
                                                        <td>Aéroport</td>
                                                        <td>Numéro de Vol</td>
                                                        <td>Heure d'Arrivée&nbsp;</td>
                                                    </tr>
                                                    <tr valign="center" class="fdc">
                                                        <td>{{ @$dossier['dateConseilleSejour1'] }}</td>
                                                        <td><input name="aeroport_depart" type="text" size="20"
                                                                   maxlength="100"
                                                                   value="{{ @$dossier['aeroport_depart'] }}">
                                                        </td>
                                                        <td><input name="num_vol_depart" type="text" size="20"
                                                                   maxlength="100"
                                                                   value="{{ @$dossier['num_vol_depart'] }}">
                                                        </td>
                                                        <td align="center"><input name="heure_depart" type="text"
                                                                                  size="6"
                                                                                  maxlength="10"
                                                                                  value="{{ @$dossier['heure_depart'] }}">
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                                <br>
                                                <table border="0" cellpadding="2" cellspacing="1"
                                                       class="table table-bordered">
                                                    <tbody>
                                                    <tr valign="top" class="info">
                                                        <td colspan="4" class="fdl1"><b>Vol retour</b></td>
                                                    </tr>
                                                    <tr align="center" valign="top" class="fdl2">
                                                        <td>Date</td>
                                                        <td>Aéroport</td>
                                                        <td>Numéro de Vol</td>
                                                        <td>Heure du Départ</td>
                                                    </tr>
                                                    <tr valign="center" class="fdc">
                                                        <td>{{ @$dossier['dateConseilleSejour2'] }}</td>
                                                        <td><input name="aeroport_arrivee" type="text" size="20"
                                                                   maxlength="100"
                                                                   value="{{ @$dossier['aeroport_arrivee'] }}">
                                                        </td>
                                                        <td><input name="num_vol_arrivee" type="text" size="20"
                                                                   maxlength="100"
                                                                   value="{{ @$dossier['num_vol_arrivee'] }}">
                                                        </td>
                                                        <td align="center"><input name="heure_arrivee" type="text"
                                                                                  size="6"
                                                                                  maxlength="10"
                                                                                  value="{{ @$dossier['heure_arrivee'] }}">
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="col-md-5"></div>
                                        </div>

                                        <p><b>Formalités à la frontière Tunisienne</b><br>
                                            Veuillez nous consulter pour vous indiquer si vous pouvez entrer en Tunisie
                                            avec
                                            votre carte d’identité ou votre passeport et si vous avez besoin ou non d’un
                                            visa.<br>
                                            A partir du 1er octobre 2014, les étrangers non-résidents en Tunisie
                                            (touristes
                                            en particulier) doivent s’acquitter d’une taxe de 30 dinars (environ 13,10
                                            Euros) lors de leur sortie du territoire tunisien.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="diagnostic">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="chirurgienTraitant">Médecin traitant:</label>
                                            <input type="text" id="chirurgienTraitant" name="chirurgienTraitant"
                                                   class="form-control" value="{{ @$dossier['chirurgienTraitant'] }}"
                                                   @if(Auth::user()->role == 'user') readonly @endif>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label for="interventionProposes">Intervention, acte ou exploration
                                                proposée:</label>
                                        <textarea class="form-control" rows="4" name="interventionProposes"
                                                  @if(Auth::user()->role == 'user') readonly @endif>{{ @$dossier['interventionProposes'] }}</textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="typeAnesthesie">Type d'anesthésie:</label>
                                            <select name="typeAnesthesie" class="form-control">
                                                <option value="-1"  @if( (@$dossier['typeAnesthesie'] != 'Générale') && (@$dossier['typeAnesthesie'] != 'Locale')) selected @endif>
                                                    Choisir le type d'anesthésie
                                                </option>
                                                <option value="Générale" @if(@$dossier['typeAnesthesie'] == 'Générale') selected @endif>
                                                    Générale
                                                </option>
                                                <option value="Locale" @if(@$dossier['typeAnesthesie'] == 'Locale') selected @endif>
                                                    Locale
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="dureeIntervention">Durée d'Intervention:</label>
                                            <input type="text" id="dureeIntervention" class="form-control"
                                                   name="dureeIntervention" value="{{ @$dossier['dureeIntervention'] }}"
                                                   @if(Auth::user()->role == 'user') readonly @endif>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="dureeHospitalisation">Durée d'Hospitalisation:</label>
                                            <input type="text" id="dureeHospitalisation" class="form-control"
                                                   name="dureeHospitalisation"
                                                   value="{{ @$dossier['dureeHospitalisation'] }}"
                                                   @if(Auth::user()->role == 'user') readonly @endif>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="dureeSejour">Durée de séjour post opératoire en Tunisie:</label>
                                            <input type="text" id="dureeSejour" class="form-control" name="dureeSejour"
                                                   value="{{ @$dossier['dureeSejour'] }}"
                                                   @if(Auth::user()->role == 'user') readonly @endif>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="dureeArretTravail">Durée d'arrêt de travail (depuis
                                                l'intervention):</label>
                                            <input type="text" id="dureeArretTravail" class="form-control"
                                                   name="dureeArretTravail" value="{{ @$dossier['dureeArretTravail'] }}"
                                                   @if(Auth::user()->role == 'user') readonly @endif>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="dateProposeeIntervention">Date proposée pour
                                                l'intervention:</label>
                                            <input type="date" id="dateProposeeIntervention" class="form-control"
                                                   name="dateProposeeIntervention"
                                                   value="{{ @$dossier['dateProposeeIntervention'] }}"
                                                   @if(Auth::user()->role == 'user') readonly @endif>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label for="diagnosticChirurgien">Diagnostic du médecin:</label>
                                        <textarea id="diagnosticChirurgien" class="form-control" rows="40"
                                                  name="diagnosticChirurgien"
                                                  @if(Auth::user()->role == 'user') readonly @endif>{{ @$dossier['diagnosticChirurgien'] }}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="devis">
                            <div class="panel panel-warning">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Votre devis</h3>
                                </div>
                                <div class="panel-body">
                                    <table class="devis-table table table-bordered">
                                        <tbody>
                                        <tr class="fdc form-inline">
                                            <td>Montant Intervention
                                                <input type="text" name="interventionDesc" class="form-control"
                                                       value="{{ @$dossier['interventionDesc'] }}"
                                                       placeholder="Désignation" size="40">
                                            </td>
                                            <td align="center">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" name="montantVols"
                                                           id="montantVols" value="{{ @$dossier['montantVols'] }}" placeholder="Montant">
                                                    <div class="input-group-addon">€</div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr class="fdc form-inline">
                                            <td>Montant Intervention
                                                <input type="text" name="interventionDesc2" class="form-control"
                                                       value="{{ @$dossier['interventionDesc2'] }}"
                                                       placeholder="Désignation" size="40">
                                            </td>
                                            <td align="center">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" name="montantInterv2"
                                                           id="montantInterv2"
                                                           value="{{ @$dossier['montantInterv2'] }}" placeholder="Montant">
                                                    <div class="input-group-addon">€</div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr class="fdc form-inline">
                                            <td>Montant Intervention
                                                <input type="text" name="interventionDesc3" class="form-control"
                                                       value="{{ @$dossier['interventionDesc3'] }}"
                                                       placeholder="Désignation" size="40">
                                            </td>
                                            <td align="center" class="">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" name="montantInterv3"
                                                           id="montantInterv3"
                                                           value="{{ @$dossier['montantInterv3'] }}" placeholder="Montant">
                                                    <div class="input-group-addon">€</div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr class="fdc">
                                            <td>Tarifs du séjour HOTEL en PC (nombre de nuits x montant de la nuitée)
                                            </td>
                                            <td align="center" class="form-inline">
                                                    <div class="input-group">
                                                        <input type="text" class="form-control" name="nombreNuits" id="nombreNuits" value="{{ @$dossier['nombreNuits'] }}">
                                                        <div class="input-group-addon">nuit(s)</div>
                                                    </div>
                                                    x
                                                    <div class="input-group">
                                                        <input type="text" class="form-control" name="montantHorsVols" id="montantHorsVols" value="{{ @$dossier['montantHorsVols'] }}">
                                                        <div class="input-group-addon">€</div>
                                                    </div>
                                            </td>
                                        </tr>
                                        <tr class="fdc">
                                            <td>Tarif séjour accompagnement HOTEL en PC (nombre de nuits x montant
                                                de la nuitée)
                                            </td>
                                            <td align="center" class="form-inline">
                                                    <div class="input-group">
                                                        <input type="text" class="form-control" name="nombreNuitsAC"
                                                               id="nombreNuitsAC"
                                                               value="{{ @$dossier['nombreNuitsAC'] }}">
                                                        <div class="input-group-addon">nuit(s)</div>
                                                    </div>
                                                    x
                                                    <div class="input-group">
                                                        <input type="text" class="form-control" name="montantHorsVolsAC"
                                                               id="montantHorsVolsAC"
                                                               value="{{ @$dossier['montantHorsVolsAC'] }}">
                                                        <div class="input-group-addon">€</div>
                                                    </div>
                                            </td>
                                        </tr>
                                        <tr class="fdc">
                                            <td>La prestation médicale et/ou chirurgicale</td>
                                            <td align="center" class="form-inline">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" name="presMedical"
                                                           id="presMedical" value="{{ @$dossier['presMedical'] }}">
                                                    <div class="input-group-addon">€</div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr class="fdc">
                                            <td>Les honoraires du médecin et de l’anesthésiste</td>
                                            <td align="center" class="form-inline">
                                                <div class="input-group">
                                                    <input type="text" class="form-control"
                                                           name="honorairesAnesthesiste"
                                                           id="honorairesAnesthesiste"
                                                           value="{{ @$dossier['honorairesAnesthesiste'] }}">
                                                    <div class="input-group-addon">€</div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr class="fdc">
                                            <td>Les consultations pré et post opératoires (si acte opératoire)</td>
                                            <td align="center" class="form-inline">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" name="conspreoperatoire"
                                                           id="conspreoperatoire"
                                                           value="{{ @$dossier['conspreoperatoire'] }}">
                                                    <div class="input-group-addon">€</div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr class="fdc">
                                            <td>Les transferts médicaux</td>
                                            <td align="center" class="form-inline">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" name="transfertsmedicaux"
                                                           id="transfertsmedicaux"
                                                           value="{{ @$dossier['transfertsmedicaux'] }}">
                                                    <div class="input-group-addon">€</div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr class="fdc">
                                            <td colspan="2">
                                                <b>Total * :
                                                    @if (  @$dossier['totalDevis'] != '' )
                                                        <b>{{ @$dossier['totalDevis'] }}</b>@else<b>0</b>@endif
                                                </b>
                                            </td>
                                            <input type="hidden" name="totalDevis"
                                                   value="{{ @$dossier['totalDevis'] }}">
                                        </tr>
                                        <tr class="fdc">
                                            <td colspan="2"><b>( * ) : Le billet d’avion ou tout autre moyen de
                                                    transport et l’assurance voyage</b></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="panel panel-warning">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Modalités de paiement</h3>
                                </div>
                                <div class="panel-body">
                                    <p><b>Est inclus dans votre devis :</b></p>
                                    <p>
                                    <ul>
                                        <li>La prestation médicale et/ou chirurgicale</li>
                                        <li>Les honoraires du médecin et de l’anesthésiste</li>
                                        <li>Les consultations pré et post opératoires (si acte opératoire)</li>
                                        <li>Les transferts médicaux</li>
                                    </ul>
                                    </p>
                                    <p>
                                        <b>N’est pas inclus dans votre devis:</b>
                                    </p>
                                    <p>
                                    <ul>
                                        <li>Le billet d’avion ou tout autre moyen de transport et l’assurance voyage
                                        </li>
                                    </ul>
                                    </p>
                                    <p><b>Validité du devis (1 mois)</b></p>
                                    <p>Six mois à compter du <input type="date" name="datePaiement"
                                                                    value="{{ @$dossier['datePaiement'] }}"></p>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="post-operatoire">
                            <div class="panel-body">
                                <div class="panel panel-warning">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">Photos avant intervention</h3>
                                    </div>
                                    <div class="panel-body">
                                        @for($i=0; $i<3; $i++)
                                            <div class="row">
                                                @for($j=1; $j<4; $j++)
                                                    <div class="col-md-4">
                                                        <p><img class="img-thumbnail" style="height: 150px;"
                                                                src="{{  @$dossier['imagePO' . ( $i * 3 + $j)]  ? route('getPhoto', ['filename' => $dossier['imagePO' . ( $i * 3 + $j)]]) : "data:image/svg+xml;charset=utf-8,%3Csvg%20xmlns%3D'http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg'%20xmlns%3Axlink%3D'http%3A%2F%2Fwww.w3.org%2F1999%2Fxlink'%20viewBox%3D'0%200%201400%20933'%3E%3Cdefs%3E%3Csymbol%20id%3D'a'%20viewBox%3D'0%200%2090%2066'%20opacity%3D'0.3'%3E%3Cpath%20d%3D'M85%205v56H5V5h80m5-5H0v66h90V0z'%2F%3E%3Ccircle%20cx%3D'18'%20cy%3D'20'%20r%3D'6'%2F%3E%3Cpath%20d%3D'M56%2014L37%2039l-8-6-17%2023h67z'%2F%3E%3C%2Fsymbol%3E%3C%2Fdefs%3E%3Cuse%20xlink%3Ahref%3D'%23a'%20width%3D'20%25'%20x%3D'40%25'%2F%3E%3C%2Fsvg%3E" }}">
                                                        </p>
                                                        <div class="form-group">
                                                            @if(Auth::user()->role != 'user')
                                                                <input type="file" name="imagePO{{ ( $i * 3) + $j }}">
                                                            @endif
                                                            <input type="hidden" name="oldImagePO{{ ( $i * 3) + $j }}"
                                                                   value="{{  @$dossier['imagePO' . ( $i * 3 + $j)] }}">
                                                        </div>
                                                    </div>
                                                @endfor
                                            </div>
                                        @endfor
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="compteRendu">Compte rendu:</label>
                                            <textarea name="compteRendu" id="compteRendu" cols="30" rows="6"
                                                      class="form-control"
                                                      @if(Auth::user()->role == 'user') readonly @endif>{{ @$dossier['compteRendu'] }}</textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="conseil">Conseil:</label>
                                        <textarea name="conseil" id="conseil" cols="30" rows="6" class="form-control"
                                                  @if(Auth::user()->role == 'user') readonly @endif>{{ @$dossier['conseil'] }}</textarea>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="planning">
                            <div class="panel-body">
                                <div class="row">
                                    @if(Auth::user()->role != 'user')
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <input type="file" name="planning">
                                                <input type="hidden" name="oldPlanning"
                                                       value="{{  @$dossier['planning'] }}">
                                            </div>
                                        </div>
                                    @else
                                        <input type="hidden" name="oldPlanning" value="{{  @$dossier['planning'] }}">
                                    @endif
                                    <div class="{{ (Auth::user()->role == 'user') ? 'col-md-12' : 'col-md-8'  }}">
                                        @if(@$dossier['planning'] != '')
                                            <iframe src="{{ route('getPhoto', ['filename' => $dossier['planning']]) }}"
                                                    width="100%" height="600" align="middle"></iframe>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="message">
                            <div class="panel-body">
                                <div class="form-group">
                                <textarea name="messageInfo" id="messageInfo" rows="6" class="form-control"
                                          @if(Auth::user()->role == 'user') readonly @endif>{{ @$dossier['messageInfo'] }}</textarea>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
            <div class="panel-footer">
                <button type="submit" class="btn btn-success">Enregistrer</button>
                <a href="{{ action('Admin\AdminController@consultationList') }}" class="btn btn-default">Annuler</a>
            </div>
        </div>
    </form>

@endsection
