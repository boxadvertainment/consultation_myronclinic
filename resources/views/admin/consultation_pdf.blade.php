@extends('admin/layout-pdf')
@section('content')
    <h1 class="page-header">
        Votre Demande
    </h1>
    <div class="content-wrapper">
        <div><strong>Numéro Dossier : </strong>{{ $consultation->id }}</div>
        <div><strong>Nom & Prénom : </strong>{{ $consultation->name }}</div>
        <div><strong>Email : </strong>{{ $consultation->email }}</div>
        <div><strong>Téléphone : </strong>{{ $consultation->phone }}</div>
        @if( @$dossier['age'] != '' )
            <div><strong>Age :</strong> {{ @$dossier['age'] }} ans</div>
        @endif
        @if( @$dossier['nationality'] != '' )
            <div><strong>Nationalité :</strong> {{ @$dossier['nationality'] }}</div>
        @endif
        @if( $consultation->pays != '' )
            <div><strong>Pays :</strong> <span class="bfh-countries" data-country="{{ $consultation->pays }}" data-flags="true"></span></div>
        @endif
        @if( @$dossier['viber'] != '' )
            <div><strong>Viber :</strong> {{ @$dossier['viber'] }}</div>
        @endif
        @if( @$dossier['whatsapp'] != '' )
            <div><strong>Whatsup : </strong>{{ @$dossier['whatsapp'] }}</div>
        @endif
        @if( @$dossier['skype'] != '' )
            <div><strong>Skype : </strong>{{ @$dossier['skype'] }}</div>
        @endif
        @if( $consultation->message != '' )
            <div><strong>Raison de la demande :</strong> {{ $consultation->message }}</div>
        @endif
        @if( @$dossier['poids'] != '' )
            <div><strong>Poids : </strong>{{ @$dossier['poids'] }}</div>
        @endif
        @if( @$dossier['sexe'] != '' )
            <div><strong>Sexe : </strong>{{ @$dossier['sexe'] }}</div>
        @endif
        @if( $dossier['remarques'] != '' )
            <div><strong>Remarques :</strong> {{ $dossier['remarques'] }}</div>
        @endif
    </div>

    @if( (@$dossier['chirurgienTraitant'] != '') || (@$dossier['interventionProposes'] != '') || (@$dossier['typeAnesthesie'] != '-1') || (@$dossier['dureeIntervention'] != '') || (@$dossier['dureeHospitalisation'] != '') || (@$dossier['dureeSejour'] != '') || (@$dossier['dureeArretTravail'] != '') || (@$dossier['dateProposeeIntervention'] != '') || (@$dossier['diagnosticChirurgien'] != '') )
        <h1 class="page-header">
            Diagnostic
        </h1>
        <div class="content-wrapper">
            @if( @$dossier['chirurgienTraitant'] != '' )
                <div><strong>Médecin traitant : </strong>{{ @$dossier['chirurgienTraitant'] }}</div>
            @endif
            @if( @$dossier['interventionProposes'] != '' )
                <div><strong>Intervention, acte ou exploration
                        proposée: </strong>{{ @$dossier['interventionProposes'] }}</div>
            @endif
            @if( @$dossier['typeAnesthesie'] != '' )
                <div><strong>Type d'anesthésie : </strong>{{ @$dossier['typeAnesthesie'] }}</div>
            @endif
            @if( @$dossier['dureeIntervention'] != '' )
                <div><strong>Durée d'Intervention : </strong>{{ @$dossier['dureeIntervention'] }}</div>
            @endif
            @if( @$dossier['dureeHospitalisation'] != '' )
                <div><strong>Durée d'Hospitalisation : </strong>{{ @$dossier['dureeHospitalisation'] }}</div>
            @endif
            @if( @$dossier['dureeSejour'] != '' )
                <div><strong>Durée de séjour post opératoire en Tunisie :</strong> {{ @$dossier['dureeSejour'] }}</div>
            @endif
            @if( @$dossier['dureeArretTravail'] != '' )
                <div><strong>Durée d'arrêt de travail (depuis l'intervention)
                        : </strong>{{ @$dossier['dureeArretTravail'] }}</div>
            @endif
            @if( @$dossier['dateProposeeIntervention'] != '' )
                <div><strong>Date proposée pour l'intervention : </strong>{{ @$dossier['dateProposeeIntervention'] }}
                </div>
            @endif
            @if( @$dossier['diagnosticChirurgien'] != '' )
                <div><strong>Diagnostic du médecin : </strong>{{ @$dossier['diagnosticChirurgien'] }}</div>
            @endif
        </div>
    @endif

    @if(  (@$dossier['consommationTabac'] != '') || (@$dossier['arreterFumer'] != '') || (@$dossier['consommationAlcool'] != '') || (@$dossier['prisMedicament'] != '') || (@$dossier['nbEnfants'] != '0') || (@$dossier['allergiqueLatex'] != '') || (@$dossier['depression'] != '') || (@$dossier['nbCesarienne'] != '0') || (@$dossier['allergique'] != '') || (@$dossier['hypertensionArterielle'] != '') || (@$dossier['diabete'] != '') || (@$dossier['cholesterol'] != '') || (@$dossier['phlebite'] != '') || (@$dossier['maladiesVirales'] != '') || (@$dossier['autresAntecedents'] != '') || (@$dossier['interventionsChirurgicales'] != '') || (@$dossier['interventionsEsthetique'] != ''))
        <h1 class="page-header">
            Vos antécédents médicaux
        </h1>
        <div class="content-wrapper">
            @if( @$dossier['consommationTabac'] != '' )
                <div><strong>Consommez-vous ou avez vous consommé du tabac ?
                        : </strong>{{ @$dossier['consommationTabac'] }}</div>
            @endif
            @if( @$dossier['consommationTabac'] == 'Oui' )
                @if( @$dossier['depuisAge'] != '' )
                    <div><strong>et depuis quel âge? </strong>{{ @$dossier['depuisAge'] }}</div>
                @endif
            @endif
            @if(  @$dossier['arreterFumer'] != '' )
                <div><strong>Avez vous arrêté de fumer ? </strong>{{ @$dossier['arreterFumer'] }}</div>
            @endif
            @if(  @$dossier['consommationAlcool'] != '' )
                <div><strong>Consommez vous de l'alcool ? </strong>{{ @$dossier['consommationAlcool'] }}</div>
            @endif
            @if(  @$dossier['consommationAlcool'] == 'Oui' )
                @if(  @$dossier['alcoolfrequence'] != '' )
                    <div><strong>Si Oui, et â quelles fréquences ? </strong>{{ @$dossier['alcoolfrequence'] }}</div>
                @endif
            @endif
            @if(  @$dossier['prisMedicament'] != '' )
                <div><strong>Prenez vous des médicaments ? </strong>{{ @$dossier['prisMedicament'] }}</div>
            @endif
            @if( @$dossier['prisMedicament'] == 'Oui' )
                @if(  @$dossier['medicamentFrequence'] != '' )
                    <div><strong>Si Oui, et â quelles fréquences ? </strong>{{ @$dossier['medicamentFrequence'] }}</div>
                @endif
                @if( @$dossier['nbGrossesses'] != '' )
                    <div><strong>Combien de grossesses avez vous eues ? </strong>{{ @$dossier['nbGrossesses'] }}</div>
                @endif
            @endif
            @if(  @$dossier['nbEnfants'] != '0' )
                <div><strong>Combien d'enfants avez vous eus ? </strong>{{ @$dossier['nbEnfants'] }}</div>
            @endif
            @if(  @$dossier['nbCesarienne'] != '0' )
                <div><strong>Combien de césarienne(s) avez vous eu ? </strong>{{ @$dossier['nbCesarienne'] }}</div>
            @endif
            @if(  @$dossier['allergique'] != '' )
                <div><strong>Etes vous allergique ?</strong> {{ @$dossier['allergique'] }}</div>
            @endif
            @if( @$dossier['allergique'] == 'Oui' )
                @if( @$dossier['autresAllergies'] != '' )
                    <div><strong>Si Oui , â quoi (merci de préciser) ? </strong>{{ @$dossier['autresAllergies'] }}</div>
                @endif
            @endif
            @if( @$dossier['allergiqueMedicament'] != '' )
                <div><strong>Etes vous allergique à un médicament ? </strong>{{ @$dossier['allergiqueMedicament'] }}
                </div>
            @endif
            @if( @$dossier['allergiqueMedicament'] != 'Oui' )
                @if( @$dossier['medicamentAllergique'] != '' )
                    <div><strong>si oui, lequel(s) ? </strong>{{ @$dossier['medicamentAllergique'] }}</div>
                @endif
            @endif
            @if(  @$dossier['allergiqueLatex'] != '' )
                <div><strong>Etes vous allergique au latex ? </strong>{{ @$dossier['allergiqueLatex'] }}</div>
            @endif
            @if( @$dossier['allergiqueLatex'] == 'Oui' )
                @if( @$dossier['autresAllergies'] != '' )
                    <div><strong>Autres allergies (merci de préciser) </strong>{{ @$dossier['autresAllergies'] }}</div>
                @endif
            @endif
            @if(  @$dossier['hypertensionArterielle'] != '' )
                <div><strong>Souffrez vous d 'hypertension artérielle
                        ? </strong>{{ @$dossier['hypertensionArterielle'] }}</div>
            @endif
            @if(  @$dossier['diabete'] != '' )
                <div><strong>Souffrez vous de diabète ? </strong>{{ @$dossier['diabete'] }}</div>
            @endif
            @if(  @$dossier['cholesterol'] != '' )
                <div><strong>Souffrez vous du cholestérol ? </strong>{{ @$dossier['cholesterol'] }}</div>
            @endif
            @if(  @$dossier['phlebite'] != '' )
                <div>Avez vous déjà présenté un caillot dans une veine de la jambe (phlébite)
                    ? </strong>{{ @$dossier['phlebite'] }}</div>
            @endif
            @if(  @$dossier['depression'] != '' )
                <div><strong>Avez vous déjà fait une dépression ? </strong>{{ @$dossier['depression'] }}</div>
            @endif
            @if(  @$dossier['depression'] == 'Oui' )
                @if( @$dossier['antidepresseur'] != ''  )
                    <div><strong>si oui, êtes vous actuellement sous antidépresseur
                            ? </strong>{{ @$dossier['antidepresseur'] }}</div>
                @endif
            @endif
            @if(  @$dossier['maladiesVirales'] != '' )
                <div><strong>Etes vous atteint(e) de maladies virales (HIV, Hépatite)
                        ? </strong>{{ @$dossier['maladiesVirales'] }}</div>
            @endif
            @if(  @$dossier['maladiesVirales'] == 'Oui' )
                @if( @$dossier['antidepresseur'] != ''  )
                    <div><strong>si oui, lesquelles ? </strong>{{ @$dossier['maladiesViralesRemarques'] }}</div>
                @endif
            @endif
            @if(  @$dossier['autresAntecedents'] != '' )
                <div><strong>Autres antécédents médicaux (Merci de préciser)
                        : </strong>{{ @$dossier['autresAntecedents'] }}</div>
            @endif
            @if(  @$dossier['interventionsChirurgicales'] != '' )
                <div><strong>Avez vous déjà eu des interventions chirurgicales
                        : </strong>{{ @$dossier['interventionsChirurgicales'] }}</div>
            @endif
            @if(  @$dossier['interventionsChirurgicales'] == 'Oui' )
                @if( @$dossier['interventionsChirurgicalesRemarques'] != ''  )
                    <div><strong>si oui, lesquelles ? </strong>{{ @$dossier['interventionsChirurgicalesRemarques'] }}
                    </div>
                @endif
            @endif
            @if(  @$dossier['interventionsEsthetique'] != '' )
                <div><strong>Avez vous déjà eu des interventions de chirurgie esthétique
                        ? </strong>{{ @$dossier['interventionsEsthetique'] }}</div>
            @endif
            @if(  @$dossier['interventionsEsthetique'] == 'Oui' )
                @if( @$dossier['interventionsEsthetiqueRemarques'] != ''  )
                    <div><strong>si oui, lesquelles ? </strong>{{ @$dossier['interventionsEsthetiqueRemarques'] }}</div>
                @endif
            @endif
        </div>
    @endif

@endsection
