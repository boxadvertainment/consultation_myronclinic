<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Box">
    <title>Admin Panel - myronclinic</title>
    <!-- Metta CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <link rel="stylesheet" href="{{ asset('css/admin/bootstrap.min.css') }}">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ asset('js/admin/summernote/summernote.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
    <link rel="stylesheet" href="{{ asset('css/bootstrap-formhelpers.css') }}">
    <link rel="stylesheet" href="{{ asset('css/tablesort.css') }}">
    <style>
        /*!
 * Start Bootstrap - Simple Sidebar HTML Template (http://startbootstrap.com)
 * Code licensed under the Apache License v2.0.
 * For details, see http://www.apache.org/licenses/LICENSE-2.0.
 */

        /* Toggle Styles */

        .panel-default {
            border-top: 0;
            border-top-left-radius: 0;
            border-top-right-radius: 0;
        }

        .country-select {
            width: 100%;
        }

        #wrapper {
            margin-top: 70px;
            -webkit-transition: all 0.5s ease;
            -moz-transition: all 0.5s ease;
            -o-transition: all 0.5s ease;
            transition: all 0.5s ease;
        }

        .page-header {
            padding-bottom: 15px;
            margin: 20px 0 20px;
        }

        /* #wrapper.toggled {
            padding-left: 250px;
        } */

        #sidebar-wrapper {
            z-index: 1000;
            position: fixed;
            left: 250px;
            top: 50px;
            width: 0;
            height: 100%;
            margin-left: -250px;
            overflow-y: auto;
            background: #3C454E;
            -webkit-transition: all 0.5s ease;
            -moz-transition: all 0.5s ease;
            -o-transition: all 0.5s ease;
            transition: all 0.5s ease;
        }

        #wrapper.toggled #sidebar-wrapper {
            width: 250px;
        }

        #page-content-wrapper {
            width: 100%;
            position: absolute;
            padding: 15px;
        }

        #wrapper.toggled #page-content-wrapper {
            position: absolute;
            margin-right: -250px;
        }

        /* Sidebar Styles */

        .sidebar-nav {
            position: absolute;
            top: 10px;
            width: 250px;
            margin: 0;
            padding: 0;
            list-style: none;
        }

        .sidebar-nav li {
            text-indent: 10px;
            line-height: 40px;
        }

        .sidebar-nav li a {
            display: block;
            text-decoration: none;
            color: #748596;
        }

        .sidebar-nav li a:hover, .sidebar-nav li.active a {
            text-decoration: none;
            color: #fff;
            background: rgba(0, 0, 0, 0.2);
        }

        .sidebar-nav li a:active,
        .sidebar-nav li a:focus {
            text-decoration: none;
        }

        .sidebar-nav > .sidebar-brand {
            height: 65px;
            font-size: 18px;
            line-height: 60px;
        }

        .sidebar-nav > .sidebar-brand a {
            color: #999999;
        }

        .sidebar-nav > .sidebar-brand a:hover {
            color: #fff;
            background: none;
        }

        @media (min-width: 768px) {
            /* #wrapper {
                padding-left: 250px;
            } */

            #wrapper.toggled {
                padding-left: 0;
            }

            #sidebar-wrapper {
                width: 250px;
            }

            #wrapper.toggled #sidebar-wrapper {
                width: 0;
            }

            #page-content-wrapper {
                padding: 20px;
                position: relative;
            }

            #wrapper.toggled #page-content-wrapper {
                position: relative;
                margin-right: 0;
            }
        }

        .sidebar {
            padding-top: 20px;
        }

        .huge {
            font-size: 32px;
        }

        .page-header {
            color: #edaf4f;
        }

        .form-group label {
            color: #000;
            font-size: 14px;
            font-weight: normal;
        }

        a {
            color: #222;
        }

        a:hover {
            color: #CF3E39;
        }

        .navbar-brand {
            padding: 10px 15px;
        }

        .navbar {
            margin-bottom: 0px !important;
        }

        .form-control[disabled], .form-control[readonly], fieldset[disabled] .form-control {
            background-color: rgba(238, 238, 238, 0.38);
            opacity: 1;
            border: 0;
            box-shadow: none;
        }

        #sidebar-wrapper .footer {
            position: absolute;
            bottom: 50px;
            padding: 10px;
            color: #9CA8B5;
        }

        #sidebar-wrapper .footer div {
            margin-bottom: 10px;
        }

        #sidebar-wrapper .footer a {
            color: #9CA8B5;
        }

        .color-1 {
            background-color: #D9EDF7;
        }

        .color-2 {
            background-color: #82A3B3;
        }
    </style>
</head>
<body>
 <nav class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">
                <img alt="Brand" height="30" class="pull-left" src="{{ asset('img/myron-logo.png') }}">
            </a>
        </div>
        <ul class="nav navbar-nav navbar-right">
            <li><a href="{{ url('auth/logout') }}">Bonjour, {{  Auth::user()->name }}&nbsp;&nbsp; <i
                            class="fa fa-sign-out"></i> Déconnexion</a></li>
        </ul>
    </div>
    {{--
    <div id="sidebar-wrapper">
        <ul class="sidebar-nav">
            <li class="active">
                <a href="{{ url('admin/consultation') }}"><i class="fa fa-comments"></i>&nbsp; Consultations</a>
            </li>
        </ul>
        <div class="footer">
            <div><a href="http://www.myronclinic.com" target=""><i class="fa fa-link"></i> www.myronclinic.com</a></div>
            <div><i class="fa fa-map-marker"></i> Rue de la feuille d’érable Résidence la brise du lac 1053 Les Berges
                du Lac 2 Tunis, Tunisie
            </div>
            <div><a href="tel:+21631305555"><i class="fa fa-phone"></i> (+216) 31 30 55 55</a></div>
            <div><a href="mailto:contact@myronclinic.com"><i class="fa fa-envelope"></i> contact@myronclinic.com</a>
            </div>
        </div>
    </div>
    --}}
</nav>
<div id="wrapper">
    @yield('content')
</div>

<!-- Modal -->
<div class="modal fade" id="consultationModal" tabindex="-1" role="dialog" aria-labelledby="DevisGratuit">
    <form id="consultation-form-modal" action="{{ url('admin/submitConsultation') }}" method="post">
        {!! csrf_field() !!}
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="title">
                        <center><img src="/img/title-form.png" alt=""/></center>
                        <br>
                        <br>
                    </div>
                    <div class="form-group">
                        <input type="text" name="name" class="form-control" placeholder="Nom et prénom" required
                               title="Ce champ est obligatoire">
                    </div>
                    <!-- Country Select from bootstrap Helpers-->
                    <div class="bfh-selectbox bfh-countries" data-country="TN" data-flags="true"
                         data-filter="true"></div>
                    <!-- End Country Select from bootstrap Helpers-->
                    <div class="form-group">
                        <input type="email" name="email" class="form-control" placeholder="Email" required
                               title="Email incorrect">
                    </div>
                    <div class="form-group">
                        <input type="tel" name="phone" class="form-control" placeholder="Téléphone" required
                               title="Téléphone incorrect">
                    </div>
                    <div class="form-group">
                        <div class="select-wrapper">
                            <select class="form-control" name="intervention" required>
                                <option value="">Interventions</option>
                                @foreach(Config::get('app.interventions') as $label => $group)
                                    <optgroup label="{{ $label }}">
                                        @foreach($group as $item => $intervention)
                                            <option value="{{ $item }}">{{ $intervention }}</option>
                                        @endforeach
                                    </optgroup>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <textarea name="message" class="form-control" rows="3" placeholder="Message"
                                  required></textarea>
                    </div>
                    <div class="form-group">

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"> Close</button>
                    <button type="submit" name="submit" class="btn btn-success submit"><i class="fa fa-check"></i>
                        Valider
                    </button>
                </div>
            </div>
        </div>
    </form>
</div>


<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="{{ asset('js/admin/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/admin/summernote/summernote.min.js') }}"></script>
<script src="{{ asset('js/admin/summernote/lang/summernote-fr-FR.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
<script src="{{ asset('js/bootstrap-formhelpers.js') }}"></script>
<script src="{{ asset('js/tablesort.js') }}"></script>

<script type="text/javascript">
    (function ($) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            error: function (jqXHR) {
                if (/<html>/i.test(jqXHR.responseText))
                    sweetAlert('Oups!', 'Une erreur serveur s\'est produite, veuillez réessayer ultérieurement.', 'error');
                else
                    sweetAlert('Oups!', jqXHR.responseText, 'error');
            }
        });

        $('table.table-sort').tablesort();

        $('[data-toggle="popover"]').popover();

        $('.consultation-form').submit(function (e) {
            $('.summernote').each(function () {
                $(this).siblings('input[type=hidden]').val($(this).summernote('code'));
            });
        });

        $('.summernote').summernote({
            lang: 'fr-FR'
        });

        $('.dropdown-menu a').click(function (e) {
            var status = $(this).data('val');
            var id = $(this).parents('td').data('id');
            $.ajax({
                url: '{{ url('admin/consultation/changeStatus') }}/' + id,
                method: 'GET',
                data: {status: status}
            }).done(function (response) {
                if (response.success) {
                    location.reload();
                } else {
                    alert('Une erreur s\'est produite, veuillez réessayer ultérieurement.');
                }

            });
            e.preventDefault();
            /* Act on the event */
        });

        $('.create-patient').click(function (e) {
            $this = $(this);
            swal({
                title: '',
                text: "Voulez-vous créer un nouveau patient ?",
                type: "info",
                showCancelButton: true,
                closeOnConfirm: false,
                showLoaderOnConfirm: true,
                cancelButtonText: 'Non',
                confirmButtonText: 'Oui'
            }, function () {
                $.ajax({
                    url: '{{ action('Admin\AdminController@consultationCreatePatient') }}',
                    method: 'POST',
                    data: {id: $this.data('id')} // + '&' + $.param({ message: $('#message').val() })
                }).done(function (response) {
                    if (response.success) {
                        swal({
                            title: '',
                            text: "Un nouveau patient a été créé avec success",
                            type: "success",
                            closeOnConfirm: false
                        }, function () {
                            location.reload();
                        });
                        return;
                    }
                    return sweetAlert({title: 'Oups!', text: response.message, type: 'error', html: true});
                });
            });
        });

        $('.devis-table input').change(function () {
            var montantInterv = Number($('#montantVols').val()) + Number($('#montantInterv2').val()) + Number($('#montantInterv3').val());
            var total = montantInterv + Number($('#nombreNuits').val()) * Number($('#montantHorsVols').val()) + Number($('#nombreNuitsAC').val()) * Number($('#montantHorsVolsAC').val());
            if (!isNaN(total)) {
                var result = total.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1 ') + ' €';
                $('#totalDevis b').text(result);
                $('#totalDevis input').val(result);
            }
        });

        $('.devis-table .money').each(function () {
            var montant = Number($(this).text());
            $(this).text(montant.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1 ') + ' €');
        });

        $('#totalDevis b').text($('#totalDevis input').val());

        if (location.hash !== '') {
            $('.nav-tabs a[href="' + location.hash + '"]').tab('show');
            $(window).scrollTop(0);
        }

        $('.nav-tabs a').on('shown.bs.tab', function (e) {
            if (history.pushState) {
                history.pushState(null, null, e.target.hash);
            }
            else {
                window.location.hash = e.target.hash
            }
        });

        $('#consultation-form-modal').submit(function (e) {
            var $this = $(this);
            $this.find('.submit').prop('disabled', true);
            $.ajax({
                url: $this.attr('action'),
                method: 'POST',
                data: $this.serialize()
            }).done(function (response) {
                if (response.success) {
                    location.reload();
                    return;
                }
                return sweetAlert({title: 'Oups!', text: response.message, type: 'error', html: true});
            });
            e.preventDefault();
        });

    })(jQuery);
</script>
<!--Start of Tawk.to Script--
<script type="text/javascript">
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
        var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
        s1.async=true;
        s1.src='https://embed.tawk.to/56a65f58f33e2e240ac9d63c/default';
        s1.charset='UTF-8';
        s1.setAttribute('crossorigin','*');
        s0.parentNode.insertBefore(s1,s0);
    })();
</script>
<!--End of Tawk.to Script-->
</body>
</html>
