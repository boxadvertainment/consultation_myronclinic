@extends('admin/layout-pdf')
@section('content')
    <h1 class="page-header">
        Votre Demande
    </h1>
    <div class="content-wrapper">
        <div><strong>Numéro Dossier : </strong>{{ $consultation->id }}</div>
        <div><strong>Nom & Prénom : </strong>{{ $consultation->name }}</div>
        @if( @$dossier['age'] != '' )
            <div><strong>Age :</strong> {{ @$dossier['age'] }} ans</div>
        @endif
        @if( @$dossier['nationality'] != '' )
            <div><strong>Nationalité :</strong> {{ @$dossier['nationality'] }}</div>
        @endif
        @if( $consultation->pays != '' )
            <div><strong>Pays :</strong> {{ $consultation->pays }}</div>
        @endif
        @if( @$dossier['viber'] != '' )
            <div><strong>Viber :</strong> {{ @$dossier['viber'] }}</div>
        @endif
        @if( @$dossier['whatsapp'] != '' )
            <div><strong>Whatsup : </strong>{{ @$dossier['whatsapp'] }}</div>
        @endif
        @if( @$dossier['skype'] != '' )
            <div><strong>Skype : </strong>{{ @$dossier['skype'] }}</div>
        @endif
        @if( $consultation->message != '' )
            <div><strong>Raison de la demande :</strong> {{ $consultation->message }}</div>
        @endif
        @if( @$dossier['poids'] != '' )
            <div><strong>Poids : </strong>{{ @$dossier['poids'] }}</div>
        @endif
        @if( @$dossier['sexe'] != '' )
            <div><strong>Sexe : </strong>{{ @$dossier['sexe'] }}</div>
        @endif
        @if( $dossier['remarques'] != '' )
            <div><strong>Remarques :</strong> {{ $dossier['remarques'] }}</div>
        @endif
    </div>

    @if(  (@$dossier['consommationTabac'] != '') || (@$dossier['arreterFumer'] != '') || (@$dossier['consommationAlcool'] != '') || (@$dossier['prisMedicament'] != '') || (@$dossier['nbEnfants'] != '') || (@$dossier['allergiqueLatex'] != '') || (@$dossier['depression'] != '') || (@$dossier['nbCesarienne'] != '') || (@$dossier['allergique'] != '') || (@$dossier['hypertensionArterielle'] != '') || (@$dossier['diabete'] != '') || (@$dossier['cholesterol'] != '') || (@$dossier['phlebite'] != '') || (@$dossier['maladiesVirales'] != '') || (@$dossier['autresAntecedents'] != '') || (@$dossier['interventionsChirurgicales'] != '') || (@$dossier['interventionsEsthetique'] != ''))
        <h1 class="page-header">
            Vos antécédents médicaux
        </h1>
        <div class="content-wrapper">
            @if( @$dossier['consommationTabac'] != '' )
                <div><strong>Consommez-vous ou avez vous consommé du tabac ?
                        : </strong>{{ @$dossier['consommationTabac'] }}</div>
            @endif
            @if( @$dossier['consommationTabac'] == 'Oui' )
                @if( @$dossier['depuisAge'] != '' )
                    <div><strong>et depuis quel âge? </strong>{{ @$dossier['depuisAge'] }}</div>
                @endif
            @endif
            @if(  @$dossier['arreterFumer'] != '' )
                <div><strong>Avez vous arrêté de fumer ? </strong>{{ @$dossier['arreterFumer'] }}</div>
            @endif
            @if(  @$dossier['consommationAlcool'] != '' )
                <div><strong>Consommez vous de l'alcool ? </strong>{{ @$dossier['consommationAlcool'] }}</div>
            @endif
            @if(  @$dossier['consommationAlcool'] == 'Oui' )
                @if(  @$dossier['alcoolfrequence'] != '' )
                    <div><strong>Si Oui, et â quelles fréquences ? </strong>{{ @$dossier['alcoolfrequence'] }}</div>
                @endif
            @endif
            @if(  @$dossier['prisMedicament'] != '' )
                <div><strong>Prenez vous des médicaments ? </strong>{{ @$dossier['prisMedicament'] }}</div>
            @endif
            @if( @$dossier['prisMedicament'] == 'Oui' )
                @if(  @$dossier['medicamentFrequence'] != '' )
                    <div><strong>Si Oui, et â quelles fréquences ? </strong>{{ @$dossier['medicamentFrequence'] }}</div>
                @endif
                @if( @$dossier['nbGrossesses'] != '' )
                    <div><strong>Combien de grossesses avez vous eues ? </strong>{{ @$dossier['nbGrossesses'] }}</div>
                @endif
            @endif
            @if(  @$dossier['nbEnfants'] != '' )
                <div><strong>Combien d'enfants avez vous eus ? </strong>{{ @$dossier['nbEnfants'] }}</div>
            @endif
            @if(  @$dossier['nbCesarienne'] != '' )
                <div><strong>Combien de césarienne(s) avez vous eu ? </strong>{{ @$dossier['nbCesarienne'] }}</div>
            @endif
            @if(  @$dossier['allergique'] != '' )
                <div><strong>Etes vous allergique ?</strong> {{ @$dossier['allergique'] }}</div>
            @endif
            @if( @$dossier['allergique'] == 'Oui' )
                @if( @$dossier['autresAllergies'] != '' )
                    <div><strong>Si Oui , â quoi (merci de préciser) ? </strong>{{ @$dossier['autresAllergies'] }}</div>
                @endif
            @endif
            @if( @$dossier['allergiqueMedicament'] != '' )
                <div><strong>Etes vous allergique à un médicament ? </strong>{{ @$dossier['allergiqueMedicament'] }}
                </div>
            @endif
            @if( @$dossier['allergiqueMedicament'] != 'Oui' )
                @if( @$dossier['medicamentAllergique'] != '' )
                    <div><strong>si oui, lequel(s) ? </strong>{{ @$dossier['medicamentAllergique'] }}</div>
                @endif
            @endif
            @if(  @$dossier['allergiqueLatex'] != '' )
                <div><strong>Etes vous allergique au latex ? </strong>{{ @$dossier['allergiqueLatex'] }}</div>
            @endif
            @if( @$dossier['allergiqueLatex'] == 'Oui' )
                @if( @$dossier['autresAllergies'] != '' )
                    <div><strong>Autres allergies (merci de préciser) </strong>{{ @$dossier['autresAllergies'] }}</div>
                @endif
            @endif
            @if(  @$dossier['hypertensionArterielle'] != '' )
                <div><strong>Souffrez vous d 'hypertension artérielle
                        ? </strong>{{ @$dossier['hypertensionArterielle'] }}</div>
            @endif
            @if(  @$dossier['diabete'] != '' )
                <div><strong>Souffrez vous de diabète ? </strong>{{ @$dossier['diabete'] }}</div>
            @endif
            @if(  @$dossier['cholesterol'] != '' )
                <div><strong>Souffrez vous du cholestérol ? </strong>{{ @$dossier['cholesterol'] }}</div>
            @endif
            @if(  @$dossier['phlebite'] != '' )
                <div>Avez vous déjà présenté un caillot dans une veine de la jambe (phlébite)
                    ? </strong>{{ @$dossier['phlebite'] }}</div>
            @endif
            @if(  @$dossier['depression'] != '' )
                <div><strong>Avez vous déjà fait une dépression ? </strong>{{ @$dossier['depression'] }}</div>
            @endif
            @if(  @$dossier['depression'] == 'Oui' )
                @if( @$dossier['antidepresseur'] != ''  )
                    <div><strong>si oui, êtes vous actuellement sous antidépresseur
                            ? </strong>{{ @$dossier['antidepresseur'] }}</div>
                @endif
            @endif
            @if(  @$dossier['maladiesVirales'] != '' )
                <div><strong>Etes vous atteint(e) de maladies virales (HIV, Hépatite)
                        ? </strong>{{ @$dossier['maladiesVirales'] }}</div>
            @endif
            @if(  @$dossier['maladiesVirales'] == 'Oui' )
                @if( @$dossier['antidepresseur'] != ''  )
                    <div><strong>si oui, lesquelles ? </strong>{{ @$dossier['maladiesViralesRemarques'] }}</div>
                @endif
            @endif
            @if(  @$dossier['autresAntecedents'] != '' )
                <div><strong>Autres antécédents médicaux (Merci de préciser)
                        : </strong>{{ @$dossier['autresAntecedents'] }}</div>
            @endif
            @if(  @$dossier['interventionsChirurgicales'] != '' )
                <div><strong>Avez vous déjà eu des interventions chirurgicales
                        : </strong>{{ @$dossier['interventionsChirurgicales'] }}</div>
            @endif
            @if(  @$dossier['interventionsChirurgicales'] == 'Oui' )
                @if( @$dossier['interventionsChirurgicalesRemarques'] != ''  )
                    <div><strong>si oui, lesquelles ? </strong>{{ @$dossier['interventionsChirurgicalesRemarques'] }}
                    </div>
                @endif
            @endif
            @if(  @$dossier['interventionsEsthetique'] != '' )
                <div><strong>Avez vous déjà eu des interventions de chirurgie esthétique
                        ? </strong>{{ @$dossier['interventionsEsthetique'] }}</div>
            @endif
            @if(  @$dossier['interventionsEsthetique'] == 'Oui' )
                @if( @$dossier['interventionsEsthetiqueRemarques'] != ''  )
                    <div><strong>si oui, lesquelles ? </strong>{{ @$dossier['interventionsEsthetiqueRemarques'] }}</div>
                @endif
            @endif
        </div>
    @endif


    @if(  (@$dossier['formuleVoyage'] != '') || (@$dossier['passeport'] != '') || (@$dossier['arrangement'] != '') || (@$dossier['typeChambre'] != '') || (@$dossier['dateArrivee'] != '') || (@$dossier['nbAdultes'] != '') || (@$dossier['nbBebes'] != '') || (@$dossier['nbEnfants'] != '') )
        <h1 class="page-header">
            Votre Voyage
        </h1>
        <div class="content-wrapper">
            @if(  @$dossier['formuleVoyage'] != '' )
                <div><strong>Hôtel : </strong>{{ @$dossier['formuleVoyage'] }}</div>
            @endif
            @if(  @$dossier['passeport'] != '' )
                <div><strong>Avez vous un passeport en cours de validité ? </strong>{{ @$dossier['passeport'] }}</div>
            @endif
            @if(  @$dossier['arrangement'] != '' )
                <div><strong>Arrangement :</strong> {{ @$dossier['arrangement'] }}</div>
            @endif
            @if(  @$dossier['typeChambre'] != '' )
                <div><strong>Type de chambre :</strong> {{ @$dossier['typeChambre'] }}</div>
            @endif
            @if(  @$dossier['dateArrivee'] != '' )
                <div><strong>Date d'arrivée : </strong>{{ @$dossier['dateArrivee'] }}</div>
            @endif
            @if(  @$dossier['nbAdultes'] != '' )
                <div><strong>Nombre d'adultes :</strong> {{ @$dossier['nbAdultes'] }}</div>
            @endif
            @if(  @$dossier['nbBebes'] != '' )
                <div><strong>Nombre de Bébés de 0 à 2 ans :</strong> {{ @$dossier['nbBebes'] }}</div>
            @endif
            @if(  @$dossier['nbEnfants'] != '' )
                <div><strong>Nombre d'enfants de 2 à 12 ans : </strong>{{ @$dossier['nbEnfants'] }}</div>
            @endif
            @if(  (@$dossier['aeroport_depart'] != '' ) && (@$dossier['num_vol_depart'] != '' ) && (@$dossier['heure_depart'] != '' ) && (@$dossier['aeroport_arrivee'] != '' ) && (@$dossier['num_vol_arrivee'] != '' ) && (@$dossier['heure_arrivee'] != '' ))
                <div><strong>Vos vols :</strong></div>
                <div class="tables">
                    @if(  (@$dossier['aeroport_depart'] != '' ) && (@$dossier['num_vol_depart'] != '' ) && (@$dossier['heure_depart'] != '' ))
                        <table border="0" cellpadding="2" cellspacing="1" class="table table-bordered">
                            <tbody>
                            <tr valign="top">
                                <td colspan="4" class="info"><b>Vol aller</b></td>
                            </tr>
                            <tr align="center" valign="top" class="fdl2">
                                @if(  @$dossier['dateConseilleSejour1'] != '' )
                                    <td>Date</td>
                                @endif
                                @if( @$dossier['aeroport_depart'] != '' )
                                    <td>Aéroport</td>
                                @endif
                                @if( @$dossier['num_vol_depart'] != '' )
                                    <td>Numéro de Vol</td>
                                @endif
                                @if( @$dossier['heure_depart'] != '' )
                                    <td>Heure du Depart ;</td>
                                @endif
                            </tr>
                            <tr valign="center" class="fdc">
                                @if( @$dossier['dateConseilleSejour1'] != '' )
                                    <td>{{ @$dossier['dateConseilleSejour1'] }}</td>
                                @endif
                                @if( @$dossier['aeroport_depart'] != '' )
                                    <td>{{ @$dossier['aeroport_depart'] }}</td>
                                @endif
                                @if( @$dossier['num_vol_depart'] != '' )
                                    <td>{{ @$dossier['num_vol_depart'] }}</td>
                                @endif
                                @if( @$dossier['heure_depart'] != '' )
                                    <td>{{ @$dossier['heure_depart'] }}</td>
                                @endif
                            </tr>
                            </tbody>
                        </table>
                    @endif
                    @if(  (@$dossier['aeroport_arrivee'] != '' ) && (@$dossier['num_vol_arrivee'] != '' ) && (@$dossier['heure_arrivee'] != '' ))
                        <table border="0" cellpadding="2" cellspacing="1" class="table table-bordered">
                            <tbody>
                            <tr valign="top">
                                <td colspan="4" class="info"><b>Vol retour</b></td>
                            </tr>
                            <tr align="center" valign="top" class="fdl2">
                                @if(  @$dossier['dateConseilleSejour2'] != '' )
                                    <td>Date</td>
                                @endif
                                @if( @$dossier['aeroport_arrivee'] != '' )
                                    <td>Aéroport</td>
                                @endif
                                @if( @$dossier['num_vol_arrivee'] != '' )
                                    <td>Numéro de Vol</td>
                                @endif
                                @if( @$dossier['heure_arrivee'] != '' )
                                    <td>Heure d'arrivée</td>
                                @endif
                            </tr>
                            <tr valign="center" class="fdc">
                                @if( @$dossier['dateConseilleSejour2'] != '' )
                                    <td>{{ @$dossier['dateConseilleSejour2'] }}</td>
                                @endif
                                @if( @$dossier['aeroport_arrivee'] != '' )
                                    <td>{{ @$dossier['aeroport_arrivee'] }}</td>
                                @endif
                                @if( @$dossier['num_vol_arrivee'] != '' )
                                    <td>{{ @$dossier['num_vol_arrivee'] }}</td>
                                @endif
                                @if( @$dossier['heure_arrivee'] != '' )
                                    <td>{{ @$dossier['heure_arrivee'] }}</td>
                                @endif
                            </tr>
                            </tbody>
                        </table>
                    @endif
                </div>
            @endif
        </div>
    @endif



    @if( (@$dossier['chirurgienTraitant'] != '') || (@$dossier['interventionProposes'] != '') || (@$dossier['typeAnesthesie'] != '') || (@$dossier['dureeIntervention'] != '') || (@$dossier['dureeHospitalisation'] != '') || (@$dossier['dureeSejour'] != '') || (@$dossier['dureeArretTravail'] != '') || (@$dossier['dateProposeeIntervention'] != '') || (@$dossier['diagnosticChirurgien'] != '') )
        <h1 class="page-header">
            Diagnostic
        </h1>
        <div class="content-wrapper">
            @if( @$dossier['chirurgienTraitant'] != '' )
                <div><strong>Médecin traitant : </strong>{{ @$dossier['chirurgienTraitant'] }}</div>
            @endif
            @if( @$dossier['interventionProposes'] != '' )
                <div><strong>Intervention, acte ou exploration
                        proposée: </strong>{{ @$dossier['interventionProposes'] }}</div>
            @endif
            @if( @$dossier['typeAnesthesie'] != '' )
                <div><strong>Type d'anesthésie : </strong>{{ @$dossier['typeAnesthesie'] }}</div>
            @endif
            @if( @$dossier['dureeIntervention'] != '' )
                <div><strong>Durée d'Intervention : </strong>{{ @$dossier['dureeIntervention'] }}</div>
            @endif
            @if( @$dossier['dureeHospitalisation'] != '' )
                <div><strong>Durée d'Hospitalisation : </strong>{{ @$dossier['dureeHospitalisation'] }}</div>
            @endif
            @if( @$dossier['dureeSejour'] != '' )
                <div><strong>Durée de séjour post opératoire en Tunisie :</strong> {{ @$dossier['dureeSejour'] }}</div>
            @endif
            @if( @$dossier['dureeArretTravail'] != '' )
                <div><strong>Durée d'arrêt de travail (depuis l'intervention)
                        : </strong>{{ @$dossier['dureeArretTravail'] }}</div>
            @endif
            @if( @$dossier['dateProposeeIntervention'] != '' )
                <div><strong>Date proposée pour l'intervention : </strong>{{ @$dossier['dateProposeeIntervention'] }}
                </div>
            @endif
            @if( @$dossier['diagnosticChirurgien'] != '' )
                <div><strong>Diagnostic du médecin : </strong>{{ @$dossier['diagnosticChirurgien'] }}</div>
            @endif
        </div>
    @endif

    @if( (@$dossier['interventionDesc'] != '') || (@$dossier['interventionDesc2'] != '') || (@$dossier['interventionDesc3'] != '') || (@$dossier['nombreNuits'] != '') || (@$dossier['nombreNuitsAC'] != '') || (@$dossier['modalitePaiement'] != '') || (@$dossier['datePaiement'] != '') )
        <h1 class="page-header">
            Votre devis
        </h1>
        <div class="content-wrapper">
            @if( @$dossier['interventionDesc'] != '' )
                <div><strong>Designation Intervention : </strong>{{ @$dossier['interventionDesc'] }}, <strong>Montant
                        : </strong>{{ @$dossier['montantVols'] }} £
                </div>
            @endif
            @if( @$dossier['interventionDesc2'] != '' )
                <div><strong>Designation Intervention : </strong>{{ @$dossier['interventionDesc2'] }}, <strong>Montant
                        : </strong>{{ @$dossier['montantInterv2'] }} £
                </div>
            @endif
            @if( @$dossier['interventionDesc3'] != '' )
                <div><strong>Designation Intervention : </strong>{{ @$dossier['interventionDesc3'] }}, <strong>Montant
                        : </strong>{{ @$dossier['montantInterv3'] }} £
                </div>
            @endif
            @if( (@$dossier['nombreNuits'] != '') && (@$dossier['montantHorsVols'] != ''))
                <div><strong>Tarifs du séjour HOTEL en PC (nombre de nuits x montant de la nuitée)
                        : </strong>{{ @$dossier['nombreNuits'] }}, {{ @$dossier['montantHorsVols'] }} £
                </div>
            @endif
            @if( (@$dossier['nombreNuitsAC'] != '') && (@$dossier['montantHorsVolsAC'] != ''))
                <div><strong>Tarif séjour accompagnement HOTEL en PC (nombre de nuits x montant de la nuitée)
                        : </strong>{{ @$dossier['nombreNuitsAC'] }} , {{ @$dossier['montantHorsVolsAC'] }} £
                </div>
            @endif
            @if( @$dossier['presMedical'] != '')
                <div><strong>La prestation médicale et/ou chirurgicale : </strong>{{ @$dossier['presMedical'] }} €</div>
            @endif
            @if( @$dossier['honorairesAnesthesiste'] != '')
                <div><strong>Les honoraires du médecin et de l’anesthésiste
                        : </strong>{{ @$dossier['honorairesAnesthesiste'] }} €
                </div>
            @endif
            @if( @$dossier['conspreoperatoire'] != '')
                <div><strong>Les consultations pré et post opératoires (si acte opératoire)
                        : </strong>{{ @$dossier['conspreoperatoire'] }} €
                </div>
            @endif
            @if( @$dossier['transfertsmedicaux'] != '')
                <div><strong>Les transferts médicaux : </strong>{{ @$dossier['transfertsmedicaux'] }} €</div>
            @endif
            @if( @$dossier['totalDevis'] != '')
                <div><strong>Total : </strong>{{ @$dossier['totalDevis'] }} €</div>
            @endif
            @if( @$dossier['modalitePaiement'] != '' )
                <div>{!! @$dossier['modalitePaiement'] !!}</div>
            @endif
            @if( @$dossier['datePaiement'] != '' )
                <div><strong>Validité du devis (1 mois) : </strong>{{ @$dossier['datePaiement'] }}</div>
            @endif
        </div>
    @endif


    @if ( (@$dossier['imagePO1'] != '') || (@$dossier['compteRendu'] != '') || (@$dossier['conseil'] != '') )
        <h1 class="page-header">
            Post Opératoire
        </h1>
        <div class="content-wrapper">
            @if ( @$dossier['imagePO1'] != '' )
                <div style="margin-bottom: 5px"><strong>Photos avant intervention : </strong></div>
                @for($i=0; $i<3; $i++)
                    <div class="row">
                        @for($j=1; $j<4; $j++)
                            @if ( @$dossier['imagePO' . ( $i * 3 + $j)] != '' )
                                <div class="col-md-4">
                                    <p>
                                        <img class="img-thumbnail"
                                             src="{{  @$dossier['imagePO' . ( $i * 3 + $j)]  ? route('getPhoto', ['filename' => $dossier['imagePO' . ( $i * 3 + $j)]]) : "data:image/svg+xml;charset=utf-8,%3Csvg%20xmlns%3D'http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg'%20xmlns%3Axlink%3D'http%3A%2F%2Fwww.w3.org%2F1999%2Fxlink'%20viewBox%3D'0%200%201400%20933'%3E%3Cdefs%3E%3Csymbol%20id%3D'a'%20viewBox%3D'0%200%2090%2066'%20opacity%3D'0.3'%3E%3Cpath%20d%3D'M85%205v56H5V5h80m5-5H0v66h90V0z'%2F%3E%3Ccircle%20cx%3D'18'%20cy%3D'20'%20r%3D'6'%2F%3E%3Cpath%20d%3D'M56%2014L37%2039l-8-6-17%2023h67z'%2F%3E%3C%2Fsymbol%3E%3C%2Fdefs%3E%3Cuse%20xlink%3Ahref%3D'%23a'%20width%3D'20%25'%20x%3D'40%25'%2F%3E%3C%2Fsvg%3E" }}">
                                    </p>
                                </div>
                            @endif
                        @endfor
                    </div>
                @endfor
            @endif
            @if(  $dossier['compteRendu'] != '' )
                <div><strong>Compte rendu : </strong>{{ @$dossier['compteRendu'] }}</div>
            @endif
            @if(  $dossier['conseil'] != '' )
                <div><strong>Conseil : </strong>{{ @$dossier['conseil'] }}</div>
            @endif
        </div>
    @endif

    @if( $dossier['planning'] != '' )
        <h1 class="page-header">
            Planning
        </h1>
        <div class="content-wrapper">
            @if(  $dossier['planning'] != '' )
                <iframe src="{{ route('getPhoto', ['filename' => $dossier['planning']]) }}" width="100%"
                        height="100%"
                        align="middle" frameborder="0"></iframe>
            @endif
        </div>
    @endif

    @if(  @$dossier['messageInfo'] )
        <h1 class="page-header">
            Message
        </h1>
        <div class="content-wrapper">
            @if(  @$dossier['messageInfo'] )
                <div><strong>Message : </strong>{{ @$dossier['messageInfo'] }}</div>
            @endif
        </div>
    @endif

@endsection
