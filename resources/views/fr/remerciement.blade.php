@extends('fr.layout')

@section('content')
          <!-- Modal -->
             <div class="modal-dialog" role="document">
                  <div class="modal-content">
                      <div class="modal-body">
                          <div class="title">
                              <center><img src="/img/title-form.png" alt="" /></center>
                              <br>
                              <br>
                              @if ($message)
                                  <p style="text-align: center; padding: 0 20px;"> {{ $message }}</p>
                              @endif
                          </div>

                      </div>
                      <div class="modal-footer">
                          <a href="/" class="form-control submit"> <i class="fa fa-times"></i> Retour </a>
                      </div>
                  </div>
              </div>


@endsection

@section('title','Chirurgie esthétique en Tunisie  - Dr Djemal : Intervention esthétique en Tunisie')
@section('description','Vous envisagez une chirurgie esthétique en Tunisie ? Dr Djemal, chirugien esthétique de renommée réaliser votre chirurgie esthétique en Tunisie')
