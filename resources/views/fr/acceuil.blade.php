@extends('fr.layout')

@section('content')
          <!-- Modal -->
          <form id="consultation-form-modal" action="{{ action('AppController@submitConsultation') }}" method="post">
              <div class="modal-dialog" role="document">
                  <div class="modal-content">
                      <div class="modal-body">
                          <div class="title">
                              <img src="/img/title-form.png" alt="Demandez une consultation">
                          </div>
                          @if ($message)
                              <p style="text-align: center; padding: 0 20px;"> {{ $message }}</p>
                          @endif
                          <input type="hidden" name="_token" value="{{ csrf_token() }}">
                          <div class="form-group">
                              <input type="text" name="name" class="form-control" placeholder="Nom et prénom"  required title="Ce champ est obligatoire">
                          </div>
                          <!-- Country Select from bootstrap Helpers-->
                          <div class="bfh-selectbox bfh-countries" data-country="TN" data-flags="true" data-filter="true"></div>
                          <!-- End Country Select from bootstrap Helpers-->
                          <div class="form-group">
                              <input type="email" name="email" class="form-control" placeholder="Email" required  title="Email incorrect">
                          </div>
                          <div class="form-group">
                              <input type="tel" name="phone" class="form-control" id="phone" placeholder="Téléphone" required title="Téléphone incorrect">
                          </div>
                          <div class="form-group">
                              <div class="select-wrapper">
                                  <select class="form-control" id="intervention" name="intervention" required>
                                      <option value="">Interventions</option>
                                      @foreach(Config::get('app.interventions') as $label => $group)
                                          <optgroup label="{{ $label }}">
                                              @foreach($group as $item => $intervention)
                                                  <option value="{{ $item }}">{{ $intervention }}</option>
                                              @endforeach
                                          </optgroup>
                                      @endforeach
                                  </select>
                              </div>
                          </div>
                          <div class="form-group">
                              <textarea name="message" class="form-control" rows="3" placeholder="Message" required></textarea>
                          </div>
                      </div>
                      <div class="modal-footer">
                          <button type="submit" name="submit" class="form-control submit" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Veuillez patientez un moment..."> <i class="fa fa-check"></i> Valider </button>
                      </div>
                  </div>
              </div>
          </form>


@endsection

@section('title','Chirurgie esthétique en Tunisie  - Dr Djemal : Intervention esthétique en Tunisie')
@section('description','Vous envisagez une chirurgie esthétique en Tunisie ? Dr Djemal, chirugien esthétique de renommée réaliser votre chirurgie esthétique en Tunisie')
