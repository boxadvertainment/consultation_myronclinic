<div class="floating-ui">

        <div class="top-header">
          <div class="container">
            <ul class="list-inline pull-left">
              <li><a href="https://www.facebook.com/Dr-Djemal-228429484155370" target="_blank"><i class="fa fa-facebook"></i></a></li>
              <li><a href="https://twitter.com/dr_djemal" target="_blank"><i class="fa fa-twitter"></i></a></li>
              <li><a href="https://tn.linkedin.com/in/tahar-djemal-131071112" target="_blank"><i class="fa fa-linkedin"></i></a></li>
            </ul>
            <ul class="list-inline pull-right">
              <li><a href="#"><i class="fa fa-phone"></i>(+216) 31 30 55 21</a></li>
              <li><a href="{{ url('contact') }}"><i class="fa fa-envelope"></i> Contact</a></li>
              {{--<li>--}}
                {{--<div class="btn-group">--}}
                  {{--<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-globe"></i> Français <span class="caret"></span></a>--}}
                  {{--<ul class="dropdown-menu">--}}
                    {{--<li><a href="{{ url('home') }}">English</a></li>--}}
                  {{--</ul>--}}
                {{--</div>--}}
              {{--</li>--}}
            </ul>
          </div>
        </div>
        <!--/.top-header -->

        <nav class="navbar">
          <div class="container">

            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="/"><img src="/img/logo.png" alt="" class="img-responsive"></a>
            </div>

            <div id="navbar" class="navbar-collapse collapse">
              <ul class="nav navbar-nav navbar-right">
                <li><a href="{{ url('/') }}" class="{{ $page == 'home' ? 'active' : ''}}">Accueil</a></li>
                <li><a href="{{ url('cv') }}" class="{{ $page == 'cv' ? 'active' : ''}}">CV</a></li>
                <li><a href="{{ url('interventions-chirurgie-esthetique') }}" class="{{ $page == 'interventions-chirurgie-esthetique' ? 'active' : ''}}">Interventions</a></li>
                <li><a href="{{ url('clinique-chirurgie-esthetique-myron') }}" class="{{ $page == 'clinique-chirurgie-esthetique-myron' ? 'active' : ''}}">Clinique Myron</a></li>
                <li><a href="{{ url('avant-apres-et-temoignages') }}" class="{{ $page == 'avant-apres-et-temoignages' ? 'active' : ''}}">Avant/Aprés</a></li>
                <li class="btn-cta-wrapper"><a href="#" class="btn-cta" data-toggle="modal" data-target="#consultationModal">Consultation</a></li>
              </ul>
            </div><!--/.nav-collapse -->

          </div><!--/.container -->
        </nav>
      </div>
