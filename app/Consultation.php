<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Consultation extends Model
{
    public function passion()
    {
        return $this->belongsTo('App\User');
    }

    public function getDateStatusAttribute($value) {
        $date_status = unserialize($value);
        if (!is_array($date_status)) {
            $date_status = [];
        }
        return $date_status;
    }

    public function setDateStatusAttribute($value) {
        $this->attributes['date_status'] = serialize($value);
    }
}
