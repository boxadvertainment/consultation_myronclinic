<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Consultation;
use App\User;
use Auth;
use Illuminate\Support\Facades\Mail;

class AdminController extends Controller
{
    public function index()
    {
        return redirect('admin/consultation');
    }

    public function consultationList()
    {

        if (Auth::user()->role == 'user') {
            $count = Consultation::where('passion_id', Auth::user()->id)->count();
            $consultations = Consultation::where('passion_id', Auth::user()->id)->paginate(100);
        } else {
            $count = Consultation::count();
            $consultations = Consultation::orderBy('created_at', 'DESC')->paginate(100);
        }

        $status = [
            'pending' => ['EN ATTENTE', 'default'],
            'devis' => ['ENVOYÉ DEVIS', 'info'],
            'confirmed' => ['CONFIRME', 'success'],
            'called' => ['TÉLÉPHONÉ', 'warning'],
            'contacted' => ['CONTACTE', 'primary'],
            'relance1' => ['RELANCE 1', 'danger'],
            'relance2' => ['RELANCE 2', 'danger'],
            'ko' => ['KO', 'danger'],
            'transfere-chirurgie-esthetique' => ['Transféré chirurgie esthétique', 'info'],
        ];

        return view('admin.consultationList', compact('consultations', 'count', 'status'));

    }

    public function consultationChangeStatus(Request $request, $id)
    {
        if (Auth::user()->role == 'user') {
            return response('Une erreur s\'est produite, veuillez réessayer ultérieurement.', 500);
        }
        $consultation = Consultation::find($id);
        $consultation->status = $request->input('status');

        $date_status = $consultation->date_status;
        $date_status[$consultation->status] = date("Y/m/d");
        $consultation->date_status = $date_status;


        if ($consultation->save()) {
            return response()->json(['success' => true]);
        }
        return response()->json(['success' => false]);
    }

    public function consultationCreatePatient(Request $request)
    {
        if (Auth::user()->role == 'user') {
            return response('Une erreur s\'est produite, veuillez réessayer ultérieurement.', 500);
        }
        $consultation = Consultation::find($request->input('id'));
        $passion = User::where('email', $consultation->email)->orWhere('name', $consultation->name)->first();
        if (!$passion) {
            $passion = new User;
            $passion->email = $consultation->email;
            $passion->name = $consultation->name;
            $passion->role = 'user';
            $passion->_password = str_random(6);
            $passion->password = bcrypt($passion->_password);
            $passion->remember_token = str_random(10);

            if (!$passion->save()) {
                return response('Une erreur s\'est produite, veuillez réessayer ultérieurement.', 500);
            }
        }

        $consultation->passion()->associate($passion);
        if ($consultation->save()) {
            return response()->json(['success' => true, 'email' => $passion->email, 'password' => $passion->_password]);
        }

        return response('Une erreur s\'est produite, veuillez réessayer ultérieurement.', 500);
    }

    public function consultationEdit($id){
        $consultation = Consultation::find($id);
        if (Auth::user()->role == 'user' && $consultation->passion_id != Auth::user()->id) {
            return response('Une erreur s\'est produite, veuillez réessayer ultérieurement.', 500);
        }
        $dossier = unserialize($consultation->dossier);

        return view('admin.consultationEdit', compact('consultation', 'dossier'));
    }

    public function consultationToPdf($id){
        $consultation = Consultation::find($id);
        if (Auth::user()->role == 'user' && $consultation->passion_id != Auth::user()->id) {
            return response('Une erreur s\'est produite, veuillez réessayer ultérieurement.', 500);
        }
        $dossier = unserialize($consultation->dossier);

        return view('admin.consultation_pdf', compact('consultation', 'dossier'));
    }

    public function info()
    {
        return view('admin.info');
    }

    public function submitConsultationEdit(Request $request, $id)
    {

        $consultation = Consultation::find($id);
        if (Auth::user()->role == 'user' && $consultation->passion_id != Auth::user()->id) {
            return response('Une erreur s\'est produite, veuillez réessayer ultérieurement.', 500);
        }
        $except[] = '_token';
        for ($i = 1; $i < 10; $i++) {
            $except[] = 'image' . $i;
            $except[] = 'imagePO' . $i;
            $except[] = 'imageRef' . $i;
            $rules['image' . $i] = 'max:8000|image';
            $rules['imagePO' . $i] = 'max:8000|image';
            $rules['imageRef' . $i] = 'max:8000|image';
        }
        $data = $request->except($except);

        $validator = \Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json(['success' => false, 'message' => implode('\n', $validator->errors()->all())]);
        }

        $consultation->name = $request->input('name');
        $consultation->email = $request->input('email');
        $consultation->phone = $request->input('phone');
        $consultation->intervention = $request->input('intervention');
        $consultation->pays = $request->input('country');
        $consultation->message = $request->input('reason');

        $directory = 'photos';
        //$path      = storage_path('app/' . $directory);
        for ($i = 1; $i < 10; $i++) {
            if ($request->hasFile('image' . $i)) {
                $file = $request->file('image' . $i);
                // Si cette instruction se trouve dans une boucle on doit utiliser la fonction microtime() au lieu de time()
                $filename = uniqid() . '_' . str_slug(str_replace($file->getClientOriginalExtension(), '', $file->getClientOriginalName())) . '.' . $file->guessExtension();
                $fileContents = \File::get($file);

                \Storage::put($directory . '/' . $filename, $fileContents);
                $data['image' . $i] = $filename;
            } else {
                $data['image' . $i] = $request->input('oldImage' . $i);
            }

            if ($request->hasFile('imagePO' . $i)) {
                $file = $request->file('imagePO' . $i);
                // Si cette instruction se trouve dans une boucle on doit utiliser la fonction microtime() au lieu de time()
                $filename = uniqid() . '_' . str_slug(str_replace($file->getClientOriginalExtension(), '', $file->getClientOriginalName())) . '.' . $file->guessExtension();
                $fileContents = \File::get($file);

                \Storage::put($directory . '/' . $filename, $fileContents);
                $data['imagePO' . $i] = $filename;
            } else {
                $data['imagePO' . $i] = $request->input('oldImagePO' . $i);
            }

            if ($request->hasFile('imageRef' . $i)) {
                $file = $request->file('imageRef' . $i);
                // Si cette instruction se trouve dans une boucle on doit utiliser la fonction microtime() au lieu de time()
                $filename = uniqid() . '_' . str_slug(str_replace($file->getClientOriginalExtension(), '', $file->getClientOriginalName())) . '.' . $file->guessExtension();
                $fileContents = \File::get($file);

                \Storage::put($directory . '/' . $filename, $fileContents);
                $data['imageRef' . $i] = $filename;
            } else {
                $data['imageRef' . $i] = $request->input('oldImageRef' . $i);
            }
        }

        if ($request->hasFile('planning')) {
            $file = $request->file('planning');
            // Si cette instruction se trouve dans une boucle on doit utiliser la fonction microtime() au lieu de time()
            $filename = uniqid() . '_' . str_slug(str_replace($file->getClientOriginalExtension(), '', $file->getClientOriginalName())) . '.' . $file->guessExtension();
            $fileContents = \File::get($file);

            \Storage::put($directory . '/' . $filename, $fileContents);
            $data['planning'] = $filename;
        } else {
            $data['planning'] = $request->input('oldPlanning');
        }
        /*$montantVols = 0;$montantInterv2=0;$montantInterv3=0;$nombreNuits=0;
        $montantHorsVols=0;$nombreNuitsAC=0;$montantHorsVolsAC=0;$presMedical=0;
        $honorairesAnesthesiste=0;$conspreoperatoire=0;$transfertsmedicaux = 0;*/
        $montantVols = $request->montantVols || 0;
        $montantInterv2 =$request->montantInterv2 || 0;
        $montantInterv3 =$request->montantInterv3 || 0;
        $nombreNuits = $request->nombreNuits || 0;
        $montantHorsVols = $request->montantHorsVols || 0;
        $nombreNuitsAC = $request->nombreNuitsAC || 0;
        $montantHorsVolsAC = $request->montantHorsVolsAC || 0;
        $presMedical = $request->presMedical || 0;
        $honorairesAnesthesiste = $request->honorairesAnesthesiste  || 0;
        $conspreoperatoire = $request->conspreoperatoire || 0;
        $transfertsmedicaux = $request->transfertsmedicaux || 0;

        $data['totalDevis'] = $montantVols + $montantInterv2 + $montantInterv3 + ($nombreNuits * $montantHorsVols) + ($nombreNuitsAC * $montantHorsVolsAC) + $presMedical + $honorairesAnesthesiste + $conspreoperatoire + $transfertsmedicaux;

        $consultation->dossier = serialize($data);

        if ($consultation->save()) {
            if (Auth::user()->role == 'user') {
                \Mail::send(['raw' => 'Le patient "' . $consultation->name . '" a apporté des modifications à son dossier'], [], function ($message) {
                    $message->from(\Config::get('app.email'), \Config::get('app.name'));
                    $message->to(\Config::get('app.email'), \Config::get('app.name'))->subject(\Config::get('app.name') . ' - Mise à jour du dossier d\'un patient');
                });
            }
            $dossier = unserialize($consultation->dossier);
            return view('admin.consultationEdit', compact('consultation', 'dossier'));
        }
        return response('Une erreur s\'est produite, veuillez réessayer ultérieurement.', 500);
    }

    public function getPhoto(Request $request, $filename)
    {
        $path = 'photos/' . ($request->input('size') ? $request->input('size') . '/' : '') . $filename;
        if (!\Storage::exists($path)) {
            abort(404);
        }

        return response(\Storage::get($path))->header('Content-Type', \Storage::mimeType($path));
    }

    public function submitConsultation(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'email',
            'message' => 'required',
            'intervention' => 'required'
        ]);

        $validator->setAttributeNames([
            'name' => 'Nom et prénom',
            'email' => 'Email',
            'phone' => 'Téléphone',
        ]);

        if ($validator->fails()) {
            return response()->json(['success' => false, 'message' => implode('<br>', $validator->errors()->all())]);
        }

        $consultation = new Consultation;

        $consultation->name = $request->input('name');
        $consultation->email = $request->input('email');
        $consultation->pays = $request->input('country');
        $consultation->phone = $request->input('phone');
        $consultation->intervention = $request->input('intervention');
        $consultation->message = $request->input('message');
        $consultation->ip = $request->ip();

        if ($consultation->save()) {
            \Mail::send(['raw' => 'Une nouvelle demande de consultation a été créé par "' . $consultation->name . '"'], [], function ($message) {
                $message->from(\Config::get('app.email'), \Config::get('app.name'));
                $message->to(\Config::get('app.email'), \Config::get('app.name'))->subject(\Config::get('app.name') . ' - Consultation Form');
            });
            return response()->json(['success' => true]);
        }
        return response('Une erreur s\'est produite, veuillez réessayer ultérieurement.', 500);
    }

    public function EmailUser(Request $request)
    {

        $admin_panel_url = "http://consultation.myronclinic.com/admin/";
        $email_user = $request->email;
        $password_user = $request->password;
        $name_user = $request->name;

        Mail::send('admin.emailUser', ['email' => $email_user, 'password' => $password_user, 'admin_panel_url' => $admin_panel_url, 'name' => $name_user], function ($message) use ($email_user, $name_user) {
            $message
                //->to($email_user,$name_user)
                ->to('wissem@box.agency', 'wissem chiha')
                ->from(\Config::get('app.email'), \Config::get('app.name'))
                ->subject('Information d\'authentification - myronclinic');

        });
        return redirect()->back();

    }

    public function deleteConsultation($id) {
        $consultation = Consultation::find($id);
        $consultation->delete();
        return redirect('admin/consultation'); 
    }

}
