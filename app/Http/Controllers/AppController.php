<?php

namespace App\Http\Controllers;

use App\Consultation;
use App\Http\Requests;
use Illuminate\Http\Request;

class AppController extends Controller
{


    public function index()
    {
        return view('fr.acceuil')->with('message', '');
    }

    public function getRemerciement($mesage) {
        return view('fr.remerciement')->with('message', $mesage);
    }

    public function submitConsultation(Request $request)
    {

        $validator = \Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'email',
            'message' => 'required',
            'intervention' => 'required',
            'country' => 'required'
        ]);

        $validator->setAttributeNames([
            'name' => 'Nom et prénom',
            'email' => 'Email',
            'phone' => 'Téléphone',
            'country' => 'Pays',
        ]);

        if ($validator->fails()) {
            return response()->json(['success' => false, 'message' => implode('<br>', $validator->errors()->all()) ]);
            //return $this->getRemerciement(implode('<br>', $validator->errors()->all()));
            //return view('fr.acceuil')->with('message', implode('<br>', $validator->errors()->all()));
        }
        $consultation = new Consultation;
        $consultation->name = $request->input('name');
        $consultation->pays = $request->input('country');
        $consultation->email = $request->input('email');
        $consultation->phone = $request->input('phone');
        $consultation->intervention = $request->input('intervention');
        $consultation->message = $request->input('message');
        $consultation->ip = $request->ip();
        if ($consultation->save()) {
            return response()->json(['success' => true, 'message' => 'Merci pour votre demande. Vous serez contacté dans les plus brefs délais.' ]);
            //return $this->getRemerciement('Merci pour votre demande. Vous serez contacté dans les plus brefs délais.');
        }
        return response(['success' => false, 'message' => 'Une erreur s\'est produite, veuillez réessayer ultérieurement.' ]);
        //return $this->getRemerciement('Une erreur est produite, veuillez réessayer ultérieurement');
        //return view('fr.acceuil')->with('message', 'Une erreur est produite, veuillez réessayer ultérieurement');
    }

}
