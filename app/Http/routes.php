<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::group([
    'prefix' => 'admin',
    'namespace' => 'Admin',
    'middleware' => 'auth'
], function()
{
    Route::get('/', 'AdminController@index');
    Route::get('info', 'AdminController@info');
    Route::get('consultation', ['uses' => 'AdminController@consultationList', 'as' => 'admin.dashboard']);
    Route::get('consultation/{id}', 'AdminController@consultationEdit');
    Route::get('consultation/print/{id}', 'AdminController@consultationToPdf');
    Route::post('consultation/{id}', 'AdminController@submitConsultationEdit');
    Route::get('consultation/changeStatus/{id}', 'AdminController@consultationChangeStatus');
    Route::post('consultation/createPatient', 'AdminController@consultationCreatePatient');
    Route::get('getPhoto/{filename}', ['uses' => 'AdminController@getPhoto', 'as' => 'getPhoto']);
    Route::post('submitConsultation', 'AdminController@submitConsultation');
    Route::get('delete-consultation/{id}', 'AdminController@deleteConsultation');
    Route::get('emailUser', 'AdminController@EmailUser');
});

Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');


// Modifier le fichier 'vendor/pragmarx/firewall/src/Firewall.php (Line: 157) => return ! ($list == 'whitelist') && $list == 'blacklist';
Route::get('/', 'AppController@index');
Route::get('merci/{message}', 'AppController@getRemerciement');
Route::post('submitConsultation', 'AppController@submitConsultation');
