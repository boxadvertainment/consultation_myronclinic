<?php

use Illuminate\Database\Seeder;
use App\User;

class UserTableSeeder extends Seeder {

    public function run()
    {
        DB::table('users')->delete();
        User::create([
            'name' => 'Super Admin',
            'role' => 'admin',
            'email' => 'contact@dr-djemal.com',
            'password' => bcrypt('drdjemal$2016')
        ]);
        User::create([
            'name' => 'Admin',
            'role' => 'admin',
            'email' => 'admin@dr-djemal.com',
            'password' => bcrypt('0000')
        ]);
        User::create([
            'name' => 'User',
            'role' => 'user',
            'email' => 'user@dr-djemal.com',
            'password' => bcrypt('0000')
        ]);
    }

}
//1   country:tn  0   2016-01-30 13:29:07 2016-01-30 13:29:07
